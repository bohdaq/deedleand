package com.deedle.loader;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import com.dd.utils.Utils;
import com.deedle.api.drive.DriveClient;
import com.deedle.utils.FileManager;

public class TimelineImageLoader
{
	private final DriveClient client = new DriveClient();

	public Runnable loadFromNetwork(final LoadDetails details)
	{
		Runnable run = new Runnable() {

			@Override
			public void run()
			{
				if (details.getImagePath().exists())
				{
					loadDrawableFromPath(details);
					return;
				}

				Bitmap bitmap = client.loadFile(details.getFileId(), details.getImageSize());
				FileManager.saveImage(details.getImagePath(), bitmap);

				String tag = (String) details.getImageView().getTag();

				if (tag.equalsIgnoreCase(details.getFileId()))
				{
					Utils.UI.setImageBitmap(details.getImageView(), bitmap);
				}

			}
		};

		return run;

	}

	public void loadFromPath(final LoadDetails details)
	{

		new AsyncTask<Void, Void, Drawable>() {
			@Override
			protected Drawable doInBackground(Void... params)
			{
				final Drawable drawable = Drawable.createFromPath(details.getImagePath().getAbsolutePath());
				return drawable;
			}

			@Override
			protected void onPostExecute(Drawable drawable)
			{
				details.getImageView().setImageDrawable(drawable);
			}
		}.execute();
	}

	private void loadDrawableFromPath(final LoadDetails details)
	{
		final Drawable drawable = Drawable.createFromPath(details.getImagePath().getAbsolutePath());

		String tag = (String) details.getImageView().getTag();

		if (tag.equalsIgnoreCase(details.getFileId()))
		{
			Utils.UI.setImageDrawable(details.getImageView(), drawable);
		}

	}
}
