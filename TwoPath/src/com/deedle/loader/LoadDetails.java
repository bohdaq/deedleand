package com.deedle.loader;

import java.io.File;

import android.widget.ImageView;

public class LoadDetails
{
	private ImageView imageView;
	private File imagePath;
	private String fileId;
	private int imageSize;

	public ImageView getImageView()
	{
		return imageView;
	}

	public void setImageView(ImageView imageView)
	{
		this.imageView = imageView;
	}

	public File getImagePath()
	{
		return imagePath;
	}

	public void setImagePath(File imagePath)
	{
		this.imagePath = imagePath;
	}

	public String getFileId()
	{
		return fileId;
	}

	public void setFileId(String fileId)
	{
		this.fileId = fileId;
	}

	public int getImageSize()
	{
		return imageSize;
	}

	public void setImageSize(int imageSize)
	{
		this.imageSize = imageSize;
	}

}
