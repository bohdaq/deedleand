package com.deedle.loader;

import java.io.File;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.deedle.api.drive.DriveClient;
import com.deedle.utils.FileManager;

public class ImageLoader
{
	private final DriveClient client = new DriveClient();

	//	public Runnable loadFromNetwork(final ImageView imageView, final File imagePath, final String fileId, final int imageSize)
	//	{
	//		Runnable run = new Runnable() {
	//
	//			@Override
	//			public void run()
	//			{
	//				Bitmap bitmap = client.loadFile(fileId, imageSize);
	//				FileManager.saveImage(imagePath, bitmap);
	//				Utils.UI.setImageBitmap(imageView, bitmap);
	//
	//			}
	//		};
	//
	//		return run;
	//
	//	}
	//
	//	public Runnable loadFromPath(final File imagePath, final ImageView imageView)
	//	{
	//
	//		Runnable run = new Runnable() {
	//
	//			@Override
	//			public void run()
	//			{
	//				final Drawable drawable = Drawable.createFromPath(imagePath.getAbsolutePath());
	//				Utils.UI.setImageDrawable(imageView, drawable);
	//			}
	//		};
	//
	//		return run;
	//
	//	}

	public void load(final ImageView imageView, final File imagePath, final String fileId, final int imageSize)
	{
		new AsyncTask<Void, Void, Drawable>() {

			@Override
			protected Drawable doInBackground(Void... params)
			{
				Drawable drawable = null;

				if (imagePath.exists())
				{
					drawable = Drawable.createFromPath(imagePath.getAbsolutePath());
				} else
				{
					Bitmap bitmap = client.loadFile(fileId, imageSize);
					FileManager.saveImage(imagePath, bitmap);
					drawable = new BitmapDrawable(imageView.getContext().getResources(), bitmap);
				}

				return drawable;
			}

			@Override
			protected void onPostExecute(Drawable drawable)
			{
				imageView.setImageDrawable(drawable);

			};

		}.execute();
	}
}
