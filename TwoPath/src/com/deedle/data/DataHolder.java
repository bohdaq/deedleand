package com.deedle.data;

import com.deedle.api.v2.deed.DeedData;
import com.deedle.api.v2.deed.FilterData;
import com.deedle.api.v2.user.UserData;

public enum DataHolder
{
	INSTANCE;

	private UserData userData;
	private DeedData deedData;
	private final FilterData filterData = new FilterData();

	public DeedData getDeedData()
	{
		if (deedData == null)
		{
			deedData = new DeedData();
		}

		return deedData;
	}

	public void setDeedData(DeedData deedData)
	{
		this.deedData = deedData;
	}

	public UserData getUserData()
	{
		return userData;
	}

	public void setUserData(UserData userData)
	{
		this.userData = userData;
	}

	public FilterData getFilterData()
	{
		return filterData;
	}

}
