package com.deedle.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.dd.utils.log.L;
import com.deedle.api.account.GoogleAccount;

public final class Pref
{

	private static final String PREF_NAME = "pref.account";
	private static final String PREF_USER_EMAIL = "user.email";
	private static final String PREF_USER_AUTHTOKEN = "user.authtoken";
	private static final String PREF_SHOW_USER_DETAIL_SCREEN = "show.user.detail.screen";

	public static boolean showUserInfoScreen(Context context)
	{

		SharedPreferences getPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		boolean showUserDetailScreen = getPreferences.getBoolean(PREF_SHOW_USER_DETAIL_SCREEN, true);

		return showUserDetailScreen;
	}

	public static void setShowUserInfoScreen(Context context, boolean showUserDetailScreen)
	{

		SharedPreferences setPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		Editor mEditor = setPreferences.edit();
		mEditor.putBoolean(PREF_SHOW_USER_DETAIL_SCREEN, showUserDetailScreen);
		mEditor.commit();

	}

	public static GoogleAccount getGoogleAccount(Context context)
	{

		SharedPreferences getPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		String email = getPreferences.getString(PREF_USER_EMAIL, null);
		String authtoken = getPreferences.getString(PREF_USER_AUTHTOKEN, null);

		GoogleAccount account = new GoogleAccount();
		account.setEmail(email);
		account.setAuthtoken(authtoken);

		return account;

	}

	public static void setGoogleAccount(Context context, GoogleAccount account)
	{
		SharedPreferences setPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		Editor mEditor = setPreferences.edit();
		mEditor.putString(PREF_USER_EMAIL, account.getEmail());
		mEditor.putString(PREF_USER_AUTHTOKEN, account.getAuthtoken());
		mEditor.commit();
	}

	public static void clearGoogleAccount(Context context)
	{
		SharedPreferences setPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		Editor mEditor = setPreferences.edit();
		mEditor.remove(PREF_USER_AUTHTOKEN);
		mEditor.remove(PREF_USER_EMAIL);
		mEditor.commit();
	}
}
