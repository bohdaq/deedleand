package com.deedle.data;

public interface Constants
{
	public static final String AUTH_TOKEN_TYPE = "oauth2:https://www.googleapis.com/auth/drive";
	//	public static final String AUTH_TOKEN_TYPE = "oauth2:https://www.googleapis.com/auth/userinfo.profile";


	public static final int USER_PHOTO = 256;
	public static final int USER_PHOTO_SMALL = 48;
	public static final int DEED_IMAGE_SMALL = 256;
	public static final int DEED_IMAGE_BIG = 500;

	public static class API
	{
		public static final String URL_SCHEMA = "http";
		public static final String URL_AUTHORITY = "deedleit.appspot.com";
		public static final String URL_API = "api";
		public static final String URL_V2 = "v2";
		public static final String URL_LOGIN = "login";
		public static final String URL_TIMELINE = "timeline";
		public static final String URL_ADD = "add";
		public static final String URL_LIKE = "like";
		public static final String URL_DISLIKE = "dislike";
		public static final String URL_FOLLOW = "follow";
		public static final String URL_UNFOLLOW = "unfollow";
		public static final String URL_COMMENT = "comment";

		public static final String JSON = "json";
		public static final String UNLIKE = "unlike";
		public static final String LIKE = "like";
		public static final String EMAIL = "email";
		public static final String NAME = "name";
		public static final String LIKED_ITEMS = "likedItems";
		public static final String FOLLOWING = "following";
		public static final String PHOTO_ID = "photoId";
		public static final String FOLDER_ID = "folderId";
		public static final String CITY = "city";
		public static final String COUNTRY = "country";
		public static final String STATUS = "status";
		public static final String COUNT = "count";
		public static final String SKIP = "skip";
		public static final String RATING = "rating";
		public static final String TYPE = "type";
		public static final String LOCATIONS = "locations";
		public static final String TITLE = "title";
		public static final String DESCRIPTION = "description";
		public static final String DATE = "date";
		public static final String LATITUDE = "latitude";
		public static final String LONGTITUDE = "longtitude";
		public static final String COMMENTS = "comments";
		public static final String COMMENT = "comment";
		public static final String EMAIL_TO_FOLLOW = "emailToFollow";
		public static final String USER_PHOTO_ID = "userPhotoId";
		public static final String DEED_ID = "deedId";


	}

	public static class Drive
	{
		public static final String URL_SCHEMA = "https";
		public static final String API_KEY = "AIzaSyBUZ2kBpB7jriuDWJJUg-u6AovL5LAA37I";
		public static final String URL_AUTHORITY = "www.googleapis.com";
		public static final String URL_PATH_FILES = "drive/v2/files";
		public static final String JSON_THUMBNAIL_LINK = "thumbnailLink";
		public static final String JSON_DOWNLOAD_URL = "downloadUrl";
		public static final String PARAMETER_FIELDS = "fields";
		public static final String PARAMETER_FIELDS_VALUE = JSON_THUMBNAIL_LINK + "," + JSON_DOWNLOAD_URL;
		public static final String PARAMETER_KEY = "key";
		public static final String ROOT_FOLDER = "Deedle";
		public static final String AVATAR = "avatar";
	}

	public static class Cache
	{

		public static final String FOLDER_SHARE = "timeline/";
		public static final String ADD_DEED_PHOTO = "share_photo";
		public static final String USER_PHOTO = "user_photo";
	}

	public static enum Extras
	{
		TITLE,
		DESCRIPTION,
		FILE_ID,
		CACHE_FOLDER,
		EVENT_TYPE;
	}

}
