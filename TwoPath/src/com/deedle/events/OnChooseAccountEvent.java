package com.deedle.events;

import com.deedle.api.account.GoogleAccount;

public interface OnChooseAccountEvent
{
	public void onChooseAccount(GoogleAccount result);
	public void onAccountNotFound();
}
