package com.deedle.events;

public interface OnLoadEvent
{
	public void onStartLoading();
	public void onEndLoading();
}
