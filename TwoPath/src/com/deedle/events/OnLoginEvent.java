package com.deedle.events;

import com.deedle.api.v2.user.UserData;

public interface OnLoginEvent
{
	public void onLogin(UserData result);

}
