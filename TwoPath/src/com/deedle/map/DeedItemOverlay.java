package com.deedle.map;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

public class DeedItemOverlay extends ItemizedOverlay<OverlayItem>
{

	private final List<OverlayItem> mOverlays = new ArrayList<OverlayItem>();
	private final OnTapEvent event;

	public DeedItemOverlay(Drawable defaultMarker, Context context, OnTapEvent event)
	{
		super(defaultMarker);
		this.event = event;
	}

	@Override
	public int size()
	{
		return mOverlays.size();
	}

	@Override
	protected OverlayItem createItem(int position)
	{
		return mOverlays.get(position);
	}

	@Override
	protected boolean onTap(int index)
	{
		event.OnTap();
		return true;
	}

	public void addOverlay(OverlayItem overlay)
	{
		mOverlays.add(overlay);
		populate();
	}

	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow)
	{
		super.draw(canvas, mapView, false);
	}

}
