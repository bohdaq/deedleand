package com.deedle.map;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View.MeasureSpec;
import android.widget.FrameLayout;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

public class CustomItem extends OverlayItem
{

	private final Drawable marker;

	public CustomItem(GeoPoint pt, String label, String snippet, FrameLayout markerLayout)
	{
		super(pt, label, snippet);

		this.marker = generateMarker(markerLayout);
	}

	public Drawable generateMarker(FrameLayout markerLayout)
	{

		Resources res = markerLayout.getContext().getResources();

		Bitmap viewCapture = null;
		Drawable drawOverlay = null;

		markerLayout.setDrawingCacheEnabled(true);
		markerLayout.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
				MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
		markerLayout.layout(0, 0, markerLayout.getMeasuredWidth(), markerLayout.getMeasuredHeight());

		markerLayout.buildDrawingCache(true);

		if (markerLayout.getDrawingCache() != null)
		{
			viewCapture = Bitmap.createBitmap(markerLayout.getDrawingCache());
			if (viewCapture != null)
			{
				markerLayout.setDrawingCacheEnabled(false);
				drawOverlay = new BitmapDrawable(res, viewCapture);
			}
		}

		return drawOverlay;
	}

	@Override
	public Drawable getMarker(int stateBitset)
	{

		if (marker != null)
		{
			marker.setBounds(-marker.getIntrinsicWidth() / 2, -marker.getIntrinsicHeight(), marker.getIntrinsicWidth() / 2, 0);
		}

		return marker;
	}

}