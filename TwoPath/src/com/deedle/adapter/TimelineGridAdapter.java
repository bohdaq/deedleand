package com.deedle.adapter;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dd.utils.log.L;
import com.deedle.R;
import com.deedle.api.v2.deed.DeedData;
import com.deedle.api.v2.user.UserData;
import com.deedle.data.Constants;
import com.deedle.loader.LoadDetails;
import com.deedle.loader.TimelineImageLoader;
import com.deedle.ui.view.VoteBtnFactory;
import com.deedle.utils.BackgroundGenerator;
import com.deedle.utils.FileManager;

public class TimelineGridAdapter extends BaseAdapter
{

	private static final class ViewHolder
	{
		TextView txtTitle;
		ImageView imgPhoto;
		LinearLayout voteBtnHolder;
	}

	private static final long TIMER_DELAY = 1000;
	private static final int FIRST_LAST_ITEM_PADDING = 24;
	private static final int ITEMS_INCREMENT = 10;

	private final ExecutorService executor = Executors.newFixedThreadPool(2);
	private final TimelineImageLoader loader = new TimelineImageLoader();
	private final FileManager fileManager;
	private final LayoutInflater inflater;
	private final Activity ac;
	private final UserData userData;
	private List<DeedData> deedList;
	private int itemCount;

	private final Handler handler = new Handler();
	private final Runnable runnable = new Runnable() {

		@Override
		public void run()
		{
			if (isLastImageLoaded())
			{
				itemCount += ITEMS_INCREMENT;

				if (itemCount > getDeedList().size())
				{
					itemCount = getDeedList().size();
					notifyDataSetChanged();
					stopTask();
					return;
				}

				notifyDataSetChanged();
			}

			handler.postDelayed(runnable, TIMER_DELAY);

		}
	};

	public TimelineGridAdapter(Activity ac, List<DeedData> deedList, UserData userData)
	{
		this.ac = ac;
		this.setDeedList(deedList);
		this.userData = userData;
		this.fileManager = new FileManager(ac);

		inflater = LayoutInflater.from(ac);

		itemCount = deedList.size() < ITEMS_INCREMENT ? deedList.size() : ITEMS_INCREMENT;

	}

	@Override
	public int getCount()
	{
		return itemCount;
	}

	@Override
	public DeedData getItem(final int thePosition)
	{
		return getDeedList().get(thePosition);
	}

	@Override
	public long getItemId(final int thePosition)
	{

		return thePosition;
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent)
	{
		View resultView = convertView;

		ViewHolder holder = null;

		if (resultView == null)
		{
			resultView = inflater.inflate(R.layout.timeline_grid_item, null);

			holder = new ViewHolder();
			holder.txtTitle = (TextView) resultView.findViewById(R.id.txtTitle);
			holder.voteBtnHolder = (LinearLayout) resultView.findViewById(R.id.voteBtnHolder);
			holder.imgPhoto = (ImageView) resultView.findViewById(R.id.imgPhoto);

			resultView.setTag(holder);
		} else
		{
			holder = (ViewHolder) resultView.getTag();
		}

		addExtraPadding(resultView, position);

		DeedData deed = getItem(position);
		holder.txtTitle.setText(deed.getTitle());

		String photoId = deed.getPhotoId();
		holder.imgPhoto.setTag(photoId);

		initVoteView(deed, holder.voteBtnHolder);

		File imagePath = fileManager.getDeedPhoto(photoId, Constants.DEED_IMAGE_SMALL);

		LoadDetails loadDetals = new LoadDetails();
		loadDetals.setImagePath(imagePath);
		loadDetals.setImageView(holder.imgPhoto);
		loadDetals.setImageSize(Constants.DEED_IMAGE_SMALL);
		loadDetals.setFileId(photoId);

		if (imagePath.exists())
		{
			loader.loadFromPath(loadDetals);
		} else
		{
			holder.imgPhoto.setImageResource(BackgroundGenerator.getDeedBackground(ac));
			executor.execute(loader.loadFromNetwork(loadDetals));
		}

		return resultView;
	}

	public void stopTask()
	{
		handler.removeCallbacks(runnable);
	}

	public void startTimer()
	{
		handler.removeCallbacks(runnable);
		handler.postDelayed(runnable, TIMER_DELAY);
	}

	public List<DeedData> getDeedList()
	{
		return deedList;
	}

	public void setDeedList(List<DeedData> deedList)
	{
		this.deedList = deedList;

		itemCount = deedList.size() < ITEMS_INCREMENT ? deedList.size() : ITEMS_INCREMENT;
	}

	private boolean isLastImageLoaded()
	{
		boolean result = false;

		DeedData deed = getItem(getCount() - 1);
		File imagePath = fileManager.getDeedPhoto(deed.getPhotoId(), Constants.DEED_IMAGE_SMALL);

		if (imagePath.exists())
		{
			result = true;
		}

		return result;
	}

	private void initVoteView(DeedData deedData, LinearLayout voteBtnHolder)
	{

		if (userVoted(deedData))
		{
			initVotedButton(deedData, voteBtnHolder);
		} else
		{
			initUnvotedButton(deedData, voteBtnHolder);
		}

	}

	private boolean userVoted(DeedData deedData)
	{
		boolean result = false;
		List<String> votedList = userData.getVotedList();
		if (votedList.contains(deedData.getPhotoId()))
		{
			result = true;
		}
		return result;
	}

	private void initUnvotedButton(DeedData deedData, LinearLayout voteBtnHolder)
	{
		voteBtnHolder.removeAllViews();

		Context context = voteBtnHolder.getContext();

		if (deedData.getLike() > deedData.getDislike())
		{
			Button likeBtn = VoteBtnFactory.createUnvoteLikeButton(context, Integer.toString(deedData.getLike()), null);
			likeBtn.setBackgroundResource(R.drawable.unvoted_normal);
			voteBtnHolder.addView(likeBtn);
		} else
		{
			Button dislikeBtn = VoteBtnFactory.createUnvoteDislikeButton(context, Integer.toString(deedData.getDislike()), null);
			dislikeBtn.setBackgroundResource(R.drawable.unvoted_normal);
			voteBtnHolder.addView(dislikeBtn);
		}

	}

	private void initVotedButton(DeedData deedData, LinearLayout voteBtnHolder)
	{
		voteBtnHolder.removeAllViews();

		Context context = voteBtnHolder.getContext();

		if (deedData.getLike() > deedData.getDislike())
		{
			Button likeBtn = VoteBtnFactory.createVoteLikeButton(context, Integer.toString(deedData.getLike()));
			voteBtnHolder.addView(likeBtn);
		} else
		{
			Button dislikeBtn = VoteBtnFactory.createVoteDislikeButton(context, Integer.toString(deedData.getDislike()));
			voteBtnHolder.addView(dislikeBtn);
		}

	}

	private void addExtraPadding(View view, int position)
	{
		if (position == 0 || position == 1)
		{
			view.setPadding(0, FIRST_LAST_ITEM_PADDING, 0, 0);
		} else if (position == getCount() - 1 || position == getCount() - 2)
		{
			view.setPadding(0, 0, 0, FIRST_LAST_ITEM_PADDING);
		} else
		{
			view.setPadding(0, 0, 0, 0);
		}

	}
}