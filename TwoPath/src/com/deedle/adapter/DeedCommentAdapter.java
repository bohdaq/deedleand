package com.deedle.adapter;

import java.io.File;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.deedle.R;
import com.deedle.api.v2.comment.CommentData;
import com.deedle.data.Constants;
import com.deedle.loader.ImageLoader;
import com.deedle.utils.FileManager;

public class DeedCommentAdapter extends BaseAdapter
{

	private static final class ViewHolder
	{
		TextView txtName;
		TextView txtComment;
		ImageView imgPhoto;
	}

	private final LayoutInflater inflater;
	private final List<CommentData> commentList;

	public DeedCommentAdapter(final Context context, final List<CommentData> commentList)
	{
		this.commentList = commentList;

		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount()
	{
		return getCommentList().size();
	}

	@Override
	public CommentData getItem(final int thePosition)
	{
		return getCommentList().get(thePosition);
	}

	@Override
	public long getItemId(final int thePosition)
	{

		return thePosition;
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent)
	{
		View resultView = convertView;

		ViewHolder holder = null;

		if (resultView == null)
		{
			resultView = inflater.inflate(R.layout.deed_comment_item, null);
			holder = new ViewHolder();
			holder.txtName = (TextView) resultView.findViewById(R.id.txtName);
			holder.txtComment = (TextView) resultView.findViewById(R.id.txtComment);
			holder.imgPhoto = (ImageView) resultView.findViewById(R.id.imgPhoto);

			resultView.setTag(holder);
		} else
		{
			holder = (ViewHolder) resultView.getTag();
		}

		CommentData comment = getItem(position);

		holder.txtComment.setText(comment.getComment());
		holder.txtName.setText(comment.getUserName());
		loadUserPhoto(holder.imgPhoto, comment.getUserPhotoId());

		return resultView;
	}

	public List<CommentData> getCommentList()
	{
		return commentList;
	}

	private void loadUserPhoto(ImageView imageView, String photId)
	{
		FileManager fileManager = new FileManager(imageView.getContext());
		File imagePath = fileManager.getDeedPhoto(photId, Constants.USER_PHOTO_SMALL);

		ImageLoader loader = new ImageLoader();
		loader.load(imageView, imagePath, photId, Constants.USER_PHOTO_SMALL);

	}
}