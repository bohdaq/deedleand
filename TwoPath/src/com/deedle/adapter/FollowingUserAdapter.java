package com.deedle.adapter;

import java.io.File;
import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.deedle.R;
import com.deedle.api.v2.user.UserClient;
import com.deedle.api.v2.user.UserData;
import com.deedle.data.Constants;
import com.deedle.loader.ImageLoader;
import com.deedle.utils.FileManager;

public class FollowingUserAdapter extends BaseAdapter
{

	private static final class ViewHolder
	{
		TextView txtName;
		TextView txtLocation;
		ImageView imgPhoto;
		Button btnRemoveUser;
	}

	private final LayoutInflater inflater;
	private final List<UserData> userList;
	private final UserData userData;

	public FollowingUserAdapter(Context context, List<UserData> commentList, UserData userData)
	{
		this.userList = commentList;
		this.userData = userData;

		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount()
	{
		return getUserList().size();
	}

	@Override
	public UserData getItem(final int thePosition)
	{
		return getUserList().get(thePosition);
	}

	@Override
	public long getItemId(final int thePosition)
	{

		return thePosition;
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent)
	{
		View resultView = convertView;

		ViewHolder holder = null;

		if (resultView == null)
		{
			resultView = inflater.inflate(R.layout.people_item, null);
			holder = new ViewHolder();
			holder.txtName = (TextView) resultView.findViewById(R.id.txtName);
			holder.txtLocation = (TextView) resultView.findViewById(R.id.txtLocation);
			holder.imgPhoto = (ImageView) resultView.findViewById(R.id.imgPhoto);
			holder.btnRemoveUser = (Button) resultView.findViewById(R.id.btnRemoveUser);

			resultView.setTag(holder);
		} else
		{
			holder = (ViewHolder) resultView.getTag();
		}

		final UserData user = getItem(position);

		holder.txtLocation.setText(user.getCountry() + "," + user.getCity());
		holder.txtName.setText(user.getName());

		holder.btnRemoveUser.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v)
			{
				getUserList().remove(position);
				notifyDataSetChanged();
				unfollowTask(user);
			}
		});

		loadUserPhoto(holder.imgPhoto, user.getPhotoId());

		return resultView;
	}

	public List<UserData> getUserList()
	{
		return userList;
	}

	private void unfollowTask(final UserData followingUser)
	{
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params)
			{
				UserClient client = new UserClient();
				client.unfollow(userData.getEmail(), followingUser.getEmail());
				userData.getFollowingEmailList().remove(followingUser.getEmail());
				return null;
			}

		}.execute();
	}

	private void loadUserPhoto(ImageView imageView, String photId)
	{
		FileManager fileManager = new FileManager(imageView.getContext());
		File imagePath = fileManager.getDeedPhoto(photId, Constants.USER_PHOTO_SMALL);

		ImageLoader loader = new ImageLoader();
		loader.load(imageView, imagePath, photId, Constants.USER_PHOTO_SMALL);

	}
}