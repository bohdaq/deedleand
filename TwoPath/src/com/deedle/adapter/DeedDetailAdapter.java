package com.deedle.adapter;

import android.app.Activity;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.deedle.api.v2.deed.DeedData;
import com.deedle.api.v2.user.UserData;
import com.deedle.ui.view.DeedCommentView;
import com.deedle.ui.view.DeedDetailView;

public class DeedDetailAdapter extends PagerAdapter
{

	private final String[] pageTitles;
	private final DeedDetailView deedDetailView;
	private final DeedCommentView deedCommentView;

	public DeedDetailAdapter(Activity activity, String[] pageTitles, DeedData deedData, UserData userData)
	{
		this.deedDetailView = new DeedDetailView(activity, deedData, userData);
		this.deedCommentView = new DeedCommentView(activity, deedData, userData);
		this.pageTitles = pageTitles;
	}

	@Override
	public int getCount()
	{
		return 2;
	}

	@Override
	public Object instantiateItem(View collection, int position)
	{

		View view = null;

		if (position == 0)
		{

			view = deedDetailView.getView();
		} else if (position == 1)
		{
			view = deedCommentView.getView();
		}

		((ViewPager) collection).addView(view, 0);

		return view;
	}

	@Override
	public void destroyItem(View arg0, int arg1, Object arg2)
	{
		((ViewPager) arg0).removeView((View) arg2);

	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1)
	{
		return arg0 == ((View) arg1);

	}

	@Override
	public Parcelable saveState()
	{
		return null;
	}

	@Override
	public CharSequence getPageTitle(int position)
	{
		String result = "";

		if (pageTitles != null && pageTitles.length == getCount())
		{
			result = pageTitles[position];
		}

		return result;
	}

}