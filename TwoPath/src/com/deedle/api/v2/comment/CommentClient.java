package com.deedle.api.v2.comment;

import static com.deedle.data.Constants.API.*;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.net.Uri;
import android.net.Uri.Builder;

import com.dd.commands.RequestCommands;
import com.dd.utils.Utils;
import com.dd.utils.log.L;
import com.deedle.api.v2.Result;
import com.deedle.api.v2.ResultHandler;
import com.deedle.api.v2.comment.CommentData;

public class CommentClient
{
	public List<CommentData> getComments(String deedId)
	{
		Builder uri = new Uri.Builder();
		uri.scheme(URL_SCHEMA);
		uri.authority(URL_AUTHORITY);
		uri.appendEncodedPath(URL_API);
		uri.appendEncodedPath(URL_V2);
		uri.appendEncodedPath(URL_COMMENT);
		uri.appendQueryParameter(DEED_ID, deedId);
		uri.build();

		CommentHandler handler = new CommentHandler();

		InputStream is = RequestCommands.Get.build(uri);

		String resultJson = Utils.IO.toString(is);
		List<CommentData> result = handler.parseComment(resultJson);

		L.i(resultJson);

		return result;
	}

	public Result comment(CommentData commentData)
	{
		Builder uri = new Uri.Builder();
		uri.scheme(URL_SCHEMA);
		uri.authority(URL_AUTHORITY);
		uri.appendEncodedPath(URL_API);
		uri.appendEncodedPath(URL_V2);
		uri.appendEncodedPath(URL_COMMENT);
		uri.build();

		CommentHandler handler = new CommentHandler();
		JSONObject json = handler.createCommentJSON(commentData);

		List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();

		BasicNameValuePair jsonValue = new BasicNameValuePair(JSON, json.toString());
		nameValuePairs.add(jsonValue);

		InputStream is = RequestCommands.Post.build(uri, nameValuePairs);

		String resultJson = Utils.IO.toString(is);
		ResultHandler resultHandler = new ResultHandler();
		Result result = resultHandler.parse(resultJson);

		L.i(resultJson);

		return result;
	}
}
