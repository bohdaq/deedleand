package com.deedle.api.v2.comment;

import static com.deedle.data.Constants.API.*;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.dd.utils.Utils;
import com.dd.utils.log.L;
import com.deedle.api.v2.ResultHandler;
import com.deedle.api.v2.comment.CommentData;

public class CommentHandler extends ResultHandler
{

	public List<CommentData> parseComment(String json)
	{
		List<CommentData> commentList = new ArrayList<CommentData>();

		try
		{
			JSONArray root = new JSONArray(json);

			for (int i = 0; i < root.length(); i++)
			{
				JSONObject comment = root.getJSONObject(i);

				String emailComment = Utils.JSON.parseString(comment, EMAIL);
				String deedId = Utils.JSON.parseString(comment, DEED_ID);
				long date = Utils.JSON.parseLong(comment, DATE);
				String userName = Utils.JSON.parseString(comment, NAME);
				String userPhotoId = Utils.JSON.parseString(comment, USER_PHOTO_ID);
				String commentComment = Utils.JSON.parseString(comment, COMMENT);

				CommentData commentData = new CommentData();
				commentData.setEmail(emailComment);
				commentData.setDeedId(deedId);
				commentData.setUserPhotoId(userPhotoId);
				commentData.setComment(commentComment);
				commentData.setUserName(userName);
				commentData.setDate(date);

				commentList.add(commentData);

			}

		} catch (JSONException e)
		{
			L.e(e.toString());
		}

		return commentList;
	}

	public JSONObject createCommentJSON(CommentData commentData)
	{
		JSONObject json = new JSONObject();

		try
		{
			json.put(EMAIL, commentData.getEmail());
			json.put(DEED_ID, commentData.getDeedId());
			json.put(COMMENT, commentData.getComment());
			json.put(USER_PHOTO_ID, commentData.getUserPhotoId());
			json.put(NAME, commentData.getUserName());
			json.put(DATE, commentData.getDate());

		} catch (JSONException e)
		{
			L.e(e.toString());
		}

		return json;
	}

}
