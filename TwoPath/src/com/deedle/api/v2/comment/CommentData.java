package com.deedle.api.v2.comment;

public class CommentData
{
	private long date;
	private String email;
	private String deedId;
	private String userPhotoId;
	private String userName;
	private String comment;

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getDeedId()
	{
		return deedId;
	}

	public void setDeedId(String deedId)
	{
		this.deedId = deedId;
	}

	public String getComment()
	{
		return comment;
	}

	public void setComment(String comment)
	{
		this.comment = comment;
	}

	public String getUserPhotoId()
	{
		return userPhotoId;
	}

	public void setUserPhotoId(String userPhotoId)
	{
		this.userPhotoId = userPhotoId;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public long getDate()
	{
		return date;
	}

	public void setDate(long date)
	{
		this.date = date;
	}

}
