package com.deedle.api.v2;

import org.json.JSONException;
import org.json.JSONObject;

import com.dd.utils.Utils;
import com.dd.utils.log.L;
import static com.deedle.data.Constants.API.*;

public class ResultHandler
{
	public Result parse(String json)
	{
		Result result = Result.UNKNOWN;

		try
		{
			JSONObject root = new JSONObject(json);
			int code = Utils.JSON.parseInt(root, STATUS);
			result = Result.parse(code);

		} catch (JSONException e)
		{
			L.e(e.toString());
		}

		return result;
	}

}
