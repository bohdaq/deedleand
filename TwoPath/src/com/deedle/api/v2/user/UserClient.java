package com.deedle.api.v2.user;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.net.Uri;
import android.net.Uri.Builder;

import com.dd.commands.RequestCommands;
import com.dd.utils.Utils;
import com.dd.utils.log.L;
import com.deedle.api.v2.Result;
import com.deedle.api.v2.ResultHandler;
import static com.deedle.data.Constants.API.*;

public class UserClient
{
	public UserData login(String email)
	{
		Builder uri = new Uri.Builder();
		uri.scheme(URL_SCHEMA);
		uri.authority(URL_AUTHORITY);
		uri.appendEncodedPath(URL_API);
		uri.appendEncodedPath(URL_V2);
		uri.appendEncodedPath(URL_LOGIN);
		uri.appendQueryParameter(EMAIL, email);
		uri.build();

		InputStream is = RequestCommands.Get.build(uri);

		String json = Utils.IO.toString(is);
		UserHandler handler = new UserHandler();
		UserData userData = handler.parseUser(json);
		return userData;

	}

	public UserData info(String email)
	{
		Builder uri = new Uri.Builder();
		uri.scheme(URL_SCHEMA);
		uri.authority(URL_AUTHORITY);
		uri.appendEncodedPath(URL_API);
		uri.appendEncodedPath(URL_V2);
		uri.appendEncodedPath(URL_LOGIN);
		uri.appendQueryParameter(EMAIL, email);
		uri.build();

		InputStream is = RequestCommands.Get.build(uri);

		String json = Utils.IO.toString(is);
		UserHandler handler = new UserHandler();
		UserData userData = handler.parseUser(json);

		return userData;

	}

	public Result update(NewUserData data)
	{
		Builder uri = new Uri.Builder();
		uri.scheme(URL_SCHEMA);
		uri.authority(URL_AUTHORITY);
		uri.appendEncodedPath(URL_API);
		uri.appendEncodedPath(URL_V2);
		uri.appendEncodedPath(URL_LOGIN);
		uri.build();

		UserHandler handler = new UserHandler();
		JSONObject json = handler.createNewUserJSON(data);

		List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();

		BasicNameValuePair jsonValue = new BasicNameValuePair(JSON, json.toString());
		nameValuePairs.add(jsonValue);

		InputStream is = RequestCommands.Post.build(uri, nameValuePairs);

		String resultJson = Utils.IO.toString(is);
		ResultHandler resultHandler = new ResultHandler();
		Result result = resultHandler.parse(resultJson);

		return result;
	}

	public Result follow(String email, String emailToFollow)
	{
		Builder uri = new Uri.Builder();
		uri.scheme(URL_SCHEMA);
		uri.authority(URL_AUTHORITY);
		uri.appendEncodedPath(URL_API);
		uri.appendEncodedPath(URL_V2);
		uri.appendEncodedPath(URL_FOLLOW);
		uri.build();

		UserHandler handler = new UserHandler();
		JSONObject json = handler.createFollowUserJSON(email, emailToFollow);

		List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();

		BasicNameValuePair jsonValue = new BasicNameValuePair(JSON, json.toString());
		nameValuePairs.add(jsonValue);

		InputStream is = RequestCommands.Post.build(uri, nameValuePairs);

		String resultJson = Utils.IO.toString(is);
		ResultHandler resultHandler = new ResultHandler();
		Result result = resultHandler.parse(resultJson);

		L.i(resultJson);

		return result;
	}

	public Result unfollow(String email, String emailToFollow)
	{
		Builder uri = new Uri.Builder();
		uri.scheme(URL_SCHEMA);
		uri.authority(URL_AUTHORITY);
		uri.appendEncodedPath(URL_API);
		uri.appendEncodedPath(URL_V2);
		uri.appendEncodedPath(URL_UNFOLLOW);
		uri.build();

		UserHandler handler = new UserHandler();
		JSONObject json = handler.createFollowUserJSON(email, emailToFollow);

		List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();

		BasicNameValuePair jsonValue = new BasicNameValuePair(JSON, json.toString());
		nameValuePairs.add(jsonValue);

		InputStream is = RequestCommands.Post.build(uri, nameValuePairs);

		String resultJson = Utils.IO.toString(is);
		ResultHandler resultHandler = new ResultHandler();
		Result result = resultHandler.parse(resultJson);

		return result;
	}

}
