package com.deedle.api.v2.user;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.dd.utils.Utils;
import com.dd.utils.log.L;
import com.deedle.api.v2.Result;
import com.deedle.api.v2.ResultHandler;
import static com.deedle.data.Constants.API.*;

public class UserHandler extends ResultHandler
{
	public UserData parseUser(String json)
	{
		UserData data = new UserData();

		try
		{

			JSONObject root = new JSONObject(json);

			String email = Utils.JSON.parseString(root, EMAIL);
			String name = Utils.JSON.parseString(root, NAME);
			String photoId = Utils.JSON.parseString(root, PHOTO_ID);
			String folderId = Utils.JSON.parseString(root, FOLDER_ID);
			String city = Utils.JSON.parseString(root, CITY);
			String country = Utils.JSON.parseString(root, COUNTRY);
			int like = Utils.JSON.parseInt(root, LIKE);
			int unlike = Utils.JSON.parseInt(root, UNLIKE);

			Result result = parse(json);

			List<String> likedIdList = new ArrayList<String>();
			List<String> followingIdList = new ArrayList<String>();

			JSONArray likedIdArr = Utils.JSON.parseArray(root, LIKED_ITEMS);

			for (int i = 0; i < likedIdArr.length(); i++)
			{
				String likedId = likedIdArr.getString(i);
				likedIdList.add(likedId);
			}

			JSONArray followingIdArr = Utils.JSON.parseArray(root, FOLLOWING);

			for (int i = 0; i < followingIdArr.length(); i++)
			{
				String likedId = followingIdArr.getString(i);
				followingIdList.add(likedId);
			}

			data.setResult(result);
			data.setEmail(email);
			data.setName(name);
			data.setCity(city);
			data.setCountry(country);
			data.setPhotoId(photoId);
			data.setFolderId(folderId);
			data.setLike(like);
			data.setUnlike(unlike);
			data.getVotedList().addAll(likedIdList);
			data.getFollowingEmailList().addAll(followingIdList);

		} catch (JSONException e)
		{
			L.e(e.toString());
		}

		return data;
	}

	public JSONObject createNewUserJSON(NewUserData data)
	{
		JSONObject json = new JSONObject();

		try
		{

			json.put(EMAIL, data.getEmail());
			json.put(NAME, data.getName());
			json.put(PHOTO_ID, data.getPhotoId());
			json.put(FOLDER_ID, data.getFolderId());
			json.put(CITY, data.getCity());
			json.put(COUNTRY, data.getCountry());

		} catch (JSONException e)
		{
			L.e(e.toString());
		}

		return json;
	}

	public JSONObject createFollowUserJSON(String email, String emailToFollow)
	{
		JSONObject json = new JSONObject();

		try
		{

			json.put(EMAIL, email);
			json.put(EMAIL_TO_FOLLOW, emailToFollow);

		} catch (JSONException e)
		{
			L.e(e.toString());
		}

		return json;
	}
}
