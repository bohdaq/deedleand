package com.deedle.api.v2.user;

import java.util.ArrayList;
import java.util.List;

public class UserData extends NewUserData
{

	private int like;
	private int unlike;
	private final List<String> votedList = new ArrayList<String>();
	private final List<String> followingEmailList = new ArrayList<String>();

	public int getLike()
	{
		return like;
	}

	public void setLike(int like)
	{
		this.like = like;
	}

	public int getUnlike()
	{
		return unlike;
	}

	public void setUnlike(int unlike)
	{
		this.unlike = unlike;
	}

	public List<String> getVotedList()
	{
		return votedList;
	}

	public List<String> getFollowingEmailList()
	{
		return followingEmailList;
	}

	public void copy(NewUserData userData)
	{
		setCity(userData.getCity());
		setCountry(userData.getCountry());
		setEmail(userData.getEmail());
		setFolderId(userData.getFolderId());
		setName(userData.getName());
		setPhotoId(userData.getPhotoId());
	}

}
