package com.deedle.api.v2;

import com.dd.utils.Utils;

public enum Result
{
	UNKNOWN,
	SUCCESS,
	UNKNOWN_ERROR,
	MISSING_PARAMETER,
	INVALID_PARAMETER,
	USER_NOT_REGISTERED;

	public class ResultCodes
	{
		public static final int UNKNOWN = -1;
		public static final int SUCCESS = 100;
		public static final int UNKNOWN_ERROR = 101;
		public static final int MISSING_PARAMETER = 102;
		public static final int INVALID_PARAMETER = 103;
		public static final int USER_NOT_REGISTERED = 104;
	}

	public static Result parse(int code)
	{
		Result result = Result.UNKNOWN;

		switch (code)
		{
			case ResultCodes.SUCCESS:
				result = SUCCESS;
				break;
			case ResultCodes.UNKNOWN_ERROR:
				result = UNKNOWN_ERROR;
				break;
			case ResultCodes.MISSING_PARAMETER:
				result = MISSING_PARAMETER;
				break;
			case ResultCodes.INVALID_PARAMETER:
				result = INVALID_PARAMETER;
				break;
			case ResultCodes.USER_NOT_REGISTERED:
				result = USER_NOT_REGISTERED;
				break;
		}

		return result;
	}

	public static Result parse(String code)
	{
		int codeParsed = Utils.Parse.parseInteger(code, -1);

		return parse(codeParsed);
	}

}
