package com.deedle.api.v2.deed;

import static com.deedle.data.Constants.API.*;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.net.Uri;
import android.net.Uri.Builder;

import com.dd.commands.RequestCommands;
import com.dd.utils.Utils;
import com.dd.utils.log.L;
import com.deedle.api.v2.Result;
import com.deedle.api.v2.ResultHandler;

public class DeedClient
{

	public List<DeedData> getDeed(FilterData data)
	{
		DeedHandler handler = new DeedHandler();
		JSONObject json = handler.createFilterJSON(data);

		Builder uri = new Uri.Builder();
		uri.scheme(URL_SCHEMA);
		uri.authority(URL_AUTHORITY);
		uri.appendEncodedPath(URL_API);
		uri.appendEncodedPath(URL_V2);
		uri.appendEncodedPath(URL_TIMELINE);
		uri.build();

		List<BasicNameValuePair> nameValue = new ArrayList<BasicNameValuePair>();
		BasicNameValuePair value = new BasicNameValuePair(JSON, json.toString());
		nameValue.add(value);

		InputStream is = RequestCommands.Post.build(uri, nameValue);

		String resultJson = Utils.IO.toString(is);
		DeedHandler resultHandler = new DeedHandler();
		List<DeedData> result = resultHandler.parseDeed(resultJson);

		return result;
	}

	public Result newDeed(NewDeedData data)
	{
		Builder uri = new Uri.Builder();
		uri.scheme(URL_SCHEMA);
		uri.authority(URL_AUTHORITY);
		uri.appendEncodedPath(URL_API);
		uri.appendEncodedPath(URL_V2);
		uri.appendEncodedPath(URL_ADD);
		uri.build();

		DeedHandler handler = new DeedHandler();
		JSONObject json = handler.createNewDeedJSON(data);

		List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();

		BasicNameValuePair jsonValue = new BasicNameValuePair(JSON, json.toString());
		nameValuePairs.add(jsonValue);

		InputStream is = RequestCommands.Post.build(uri, nameValuePairs);

		String resultJson = Utils.IO.toString(is);
		ResultHandler resultHandler = new ResultHandler();
		Result result = resultHandler.parse(resultJson);

		return result;
	}

	public Result like(String email, String photoId)
	{
		Builder uri = new Uri.Builder();
		uri.scheme(URL_SCHEMA);
		uri.authority(URL_AUTHORITY);
		uri.appendEncodedPath(URL_API);
		uri.appendEncodedPath(URL_V2);
		uri.appendEncodedPath(URL_LIKE);
		uri.build();

		DeedHandler handler = new DeedHandler();
		JSONObject json = handler.createVoteJSON(email, photoId);

		List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();

		BasicNameValuePair jsonValue = new BasicNameValuePair(JSON, json.toString());
		nameValuePairs.add(jsonValue);

		InputStream is = RequestCommands.Post.build(uri, nameValuePairs);

		String resultJson = Utils.IO.toString(is);
		ResultHandler resultHandler = new ResultHandler();
		Result result = resultHandler.parse(resultJson);

		return result;
	}

	public Result dislike(String email, String photoId)
	{
		Builder uri = new Uri.Builder();
		uri.scheme(URL_SCHEMA);
		uri.authority(URL_AUTHORITY);
		uri.appendEncodedPath(URL_API);
		uri.appendEncodedPath(URL_V2);
		uri.appendEncodedPath(URL_DISLIKE);
		uri.build();

		DeedHandler handler = new DeedHandler();
		JSONObject json = handler.createVoteJSON(email, photoId);

		List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();

		BasicNameValuePair jsonValue = new BasicNameValuePair(JSON, json.toString());
		nameValuePairs.add(jsonValue);

		InputStream is = RequestCommands.Post.build(uri, nameValuePairs);

		String resultJson = Utils.IO.toString(is);
		ResultHandler resultHandler = new ResultHandler();
		Result result = resultHandler.parse(resultJson);

		return result;
	}

}
