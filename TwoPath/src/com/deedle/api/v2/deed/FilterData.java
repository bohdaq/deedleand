package com.deedle.api.v2.deed;


public class FilterData
{
	private String email;
	private int count;
	private int skip;
	private String city;
	private String country;
	private Rating rating;
	private Type type;

	public enum Rating
	{
		LIKE,
		UNLIKE,
		ALL;
	}

	public enum Type
	{
		MINE,
		FOLLOWING,
		ALL;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public int getCount()
	{
		return count;
	}

	public void setCount(int count)
	{
		this.count = count;
	}

	public int getSkip()
	{
		return skip;
	}

	public void setSkip(int skip)
	{
		this.skip = skip;
	}

	public Rating getRating()
	{
		return rating;
	}

	public void setRating(Rating rating)
	{
		this.rating = rating;
	}

	public Type getType()
	{
		return type;
	}

	public void setType(Type type)
	{
		this.type = type;
	}

}
