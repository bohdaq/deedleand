package com.deedle.api.v2.deed;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class NewDeedData
{
	private long date;
	private double latitude;
	private double longtitude;
	private String email;
	private String name;
	private String userPhotoId;
	private String title;
	private String description;
	private String photoId;
	private String city;
	private String country;

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public long getDate()
	{
		return date;
	}

	public void setDate(long date)
	{
		this.date = date;
	}

	public double getLatitude()
	{
		return latitude;
	}

	public void setLatitude(double latitude)
	{
		this.latitude = latitude;
	}

	public double getLongtitude()
	{
		return longtitude;
	}

	public void setLongtitude(double longtitude)
	{
		this.longtitude = longtitude;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getPhotoId()
	{
		return photoId;
	}

	public void setPhotoId(String photoId)
	{
		this.photoId = photoId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getUserPhotoId()
	{
		return userPhotoId;
	}

	public void setUserPhotoId(String userPhotoId)
	{
		this.userPhotoId = userPhotoId;
	}
}
