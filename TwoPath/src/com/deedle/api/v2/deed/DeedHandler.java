package com.deedle.api.v2.deed;

import static com.deedle.data.Constants.API.*;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.dd.utils.Utils;
import com.dd.utils.log.L;
import com.deedle.api.v2.ResultHandler;

public class DeedHandler extends ResultHandler
{

	public List<DeedData> parseDeed(String json)
	{
		List<DeedData> deedList = new ArrayList<DeedData>();

		try
		{
			JSONArray root = new JSONArray(json);

			for (int i = 0; i < root.length(); i++)
			{
				JSONObject deed = root.getJSONObject(i);

				String title = Utils.JSON.parseString(deed, TITLE);
				String email = Utils.JSON.parseString(deed, EMAIL);
				String description = Utils.JSON.parseString(deed, DESCRIPTION);
				String photoId = Utils.JSON.parseString(deed, PHOTO_ID);
				String city = Utils.JSON.parseString(deed, CITY);
				String country = Utils.JSON.parseString(deed, COUNTRY);
				String name = Utils.JSON.parseString(deed, NAME);
				String userPhotoId = Utils.JSON.parseString(deed, USER_PHOTO_ID);

				long date = Utils.JSON.parseLong(deed, DATE);
				int like = Utils.JSON.parseInt(deed, LIKE);
				int unlike = Utils.JSON.parseInt(deed, UNLIKE);

				float latitude = (float) Utils.JSON.parseDouble(deed, LATITUDE);
				float longtitude = (float) Utils.JSON.parseDouble(deed, LONGTITUDE);

				DeedData deedData = new DeedData();
				deedData.setTitle(title);
				deedData.setEmail(email);
				deedData.setDescription(description);
				deedData.setPhotoId(photoId);
				deedData.setCity(city);
				deedData.setCountry(country);
				deedData.setDate(date);
				deedData.setName(name);
				deedData.setUserPhotoId(userPhotoId);
				deedData.setLike(like);
				deedData.setUnlike(unlike);
				deedData.setLatitude(latitude);
				deedData.setLongtitude(longtitude);

				deedList.add(deedData);

			}

		} catch (JSONException e)
		{
			L.e(e.toString());
		}

		return deedList;
	}

	public JSONObject createVoteJSON(String email, String photoId)
	{
		JSONObject json = new JSONObject();

		try
		{
			json.put(EMAIL, email);
			json.put(PHOTO_ID, photoId);

		} catch (JSONException e)
		{
			L.e(e.toString());
		}

		return json;
	}

	public JSONObject createFilterJSON(FilterData data)
	{
		JSONObject json = new JSONObject();

		try
		{

			json.put(EMAIL, data.getEmail());
			json.put(COUNT, data.getCount());
			json.put(SKIP, data.getSkip());
			json.put(RATING, data.getRating().toString().toLowerCase());
			json.put(TYPE, data.getType().toString().toLowerCase());

			JSONObject obj = new JSONObject();
			obj.put(CITY, Utils.JSON.validateNull(data.getCity()));
			obj.put(COUNTRY, Utils.JSON.validateNull(data.getCountry()));

			json.put(LOCATIONS, obj);

		} catch (JSONException e)
		{
			L.e(e.toString());
		}

		return json;
	}

	public JSONObject createNewDeedJSON(NewDeedData data)
	{
		JSONObject json = new JSONObject();

		try
		{
			json.put(EMAIL, data.getEmail());
			json.put(TITLE, data.getTitle());
			json.put(DESCRIPTION, data.getDescription());
			json.put(PHOTO_ID, data.getPhotoId());
			json.put(DATE, data.getDate());
			json.put(CITY, Utils.JSON.validateNull(data.getCity()));
			json.put(COUNTRY, Utils.JSON.validateNull(data.getCountry()));
			json.put(NAME, data.getName());
			json.put(USER_PHOTO_ID, data.getUserPhotoId());
			json.put(LATITUDE, data.getLatitude());
			json.put(LONGTITUDE, data.getLongtitude());

		} catch (JSONException e)
		{
			L.e(e.toString());
		}

		return json;
	}
}
