package com.deedle.api.v2.deed;

public class DeedData extends NewDeedData
{

	private int like;
	private int unlike;

	public int getLike()
	{
		return like;
	}

	public void setLike(int like)
	{
		this.like = like;
	}

	public int getDislike()
	{
		return unlike;
	}

	public void setUnlike(int unlike)
	{
		this.unlike = unlike;
	}

}
