package com.deedle.api.drive;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.net.Uri.Builder;

import com.dd.commands.RequestCommands;
import com.dd.utils.Utils;
import com.dd.utils.log.L;
import com.deedle.data.Constants;
import com.deedle.utils.ThreadUtils;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.http.json.JsonHttpRequest;
import com.google.api.client.http.json.JsonHttpRequestInitializer;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveRequest;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.ParentReference;
import com.google.api.services.drive.model.Permission;

public class DriveClient
{

	public Bitmap loadFile(String fileId, int imageSize)
	{
		Builder uri = new Uri.Builder();
		uri.scheme(Constants.Drive.URL_SCHEMA);
		uri.authority(Constants.Drive.URL_AUTHORITY);
		uri.appendEncodedPath(Constants.Drive.URL_PATH_FILES);
		uri.appendEncodedPath(fileId);
		uri.appendQueryParameter(Constants.Drive.PARAMETER_FIELDS, Constants.Drive.PARAMETER_FIELDS_VALUE);
		uri.appendQueryParameter(Constants.Drive.PARAMETER_KEY, Constants.Drive.API_KEY);
		uri.build();

		InputStream is = RequestCommands.Get.build(uri);

		String string = Utils.IO.toString(is);

		Bitmap bitmap = null;

		try
		{
			JSONObject jObejct = new JSONObject(string);

			String url = jObejct.getString(Constants.Drive.JSON_THUMBNAIL_LINK);
			url = prepareUrl(url, imageSize);
			is = RequestCommands.Get.build(url);
			bitmap = BitmapFactory.decodeStream(is);

		} catch (JSONException e)
		{
			L.e(e.toString());
		}

		return bitmap;
	}

	private String prepareUrl(String url, int imageSize)
	{
		String pattern = "=s";
		int index = url.lastIndexOf(pattern);
		url = url.substring(0, index + pattern.length());
		url += imageSize;

		return url;
	}

	public File uploadFile(String authToken, String apiKey, String filePath, String title, String description, String folderId)
	{

		int tryCount = 0;

		File uploadedFile = null;

		while (uploadedFile == null && tryCount < 10)
		{
			try
			{
				tryCount++;

				Drive drive = buildService(authToken, apiKey);

				FileContent mediaContent = new FileContent("image/jpg", new java.io.File(filePath));

				File file = new File();
				file.setTitle(title);
				file.setDescription(description);

				List<ParentReference> parent = new ArrayList<ParentReference>();
				ParentReference pr = new ParentReference();
				pr.setId(folderId);
				parent.add(pr);

				file.setParents(parent);

				uploadedFile = drive.files().insert(file, mediaContent).execute();

			} catch (IOException e)
			{
				L.e(e.toString());
				ThreadUtils.wait(this, 500);
			}
		}

		return uploadedFile;
	}

	public String createFolder(final String authToken, final String apiKey)
	{
		Drive drive = buildService(authToken, apiKey);

		File folder = new File();
		folder.setTitle(Constants.Drive.ROOT_FOLDER);
		folder.setMimeType("application/vnd.google-apps.folder");

		int tryCount = 0;

		String folderId = null;

		while (folderId == null && tryCount < 10)
		{
			try
			{
				folderId = drive.files().insert(folder).execute().getId();
			} catch (IOException e)
			{
				L.e(e.toString());
				ThreadUtils.wait(this, 500);
			}
		}

		if (folderId != null)
		{
			shareFile(drive, folderId, null, "anyone", "reader");
		}

		return folderId;
	}

	private void shareFile(Drive service, String fileId, String value, String type, String role)
	{
		Permission newPermission = new Permission();

		newPermission.setValue(value);
		newPermission.setType(type);
		newPermission.setRole(role);
		try
		{
			service.permissions().insert(fileId, newPermission).execute();
		} catch (IOException e)
		{
			L.e(e.toString());
		}
	}

	private Drive buildService(final String AuthToken, final String ApiKey)
	{
		HttpTransport httpTransport = new NetHttpTransport();
		JacksonFactory jsonFactory = new JacksonFactory();

		Drive.Builder b = new Drive.Builder(httpTransport, jsonFactory, null);

		b.setJsonHttpRequestInitializer(new JsonHttpRequestInitializer() {

			@Override
			public void initialize(JsonHttpRequest request) throws IOException
			{
				DriveRequest driveRequest = (DriveRequest) request;
				driveRequest.setPrettyPrint(true);
				driveRequest.setKey(ApiKey);
				driveRequest.setOauthToken(AuthToken);
			}
		});

		return b.build();
	}

	public static String getDriveFileDescription(String title, String description, long date)
	{
		SimpleDateFormat dateFormatt = new SimpleDateFormat("yyyy MMM dd HH:mm");
		StringBuilder result = new StringBuilder();

		result.append("Title: ");
		result.append(title);
		result.append("Description: ");
		result.append(description);
		result.append("Date: ");
		result.append(dateFormatt.format(new Date(date)));

		return result.toString();
	}
}
