package com.deedle.api.account;

public class GoogleAccount
{
	private String email;
	private String authtoken;

	public String getAuthtoken()
	{
		return authtoken;
	}

	public void setAuthtoken(String authtoken)
	{
		this.authtoken = authtoken;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public static boolean isValid(GoogleAccount account)
	{
		return account != null && account.getAuthtoken() != null && account.getEmail() != null;
	}
}
