package com.deedle.api.account;

import java.io.IOException;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import com.dd.utils.log.L;
import com.deedle.R;
import com.deedle.data.Constants;
import com.deedle.events.OnChooseAccountEvent;
import com.google.api.client.googleapis.extensions.android2.auth.GoogleAccountManager;

public class GoogleAccountClient implements AccountManagerCallback<Bundle>
{
	private OnChooseAccountEvent listener;
	private Account currAccount;
	private Activity activity;

	@Override
	public void run(AccountManagerFuture<Bundle> future)
	{
		try
		{

			String name = future.getResult().getString(AccountManager.KEY_ACCOUNT_NAME);
			String authtoken = future.getResult().getString(AccountManager.KEY_AUTHTOKEN);

			GoogleAccount account = new GoogleAccount();
			account.setEmail(name);
			account.setAuthtoken(authtoken);

			listener.onChooseAccount(account);

		} catch (OperationCanceledException e)
		{
			L.e(e.toString());
		} catch (AuthenticatorException e)
		{
			L.e(e.toString());
		} catch (IOException e)
		{
			getAuthToken();
			L.e(e.toString());
		}

	}

	public void chooseAccount(final Activity activity, OnChooseAccountEvent listener)
	{
		this.listener = listener;
		this.activity = activity;

		final GoogleAccountManager googleAccountManager = new GoogleAccountManager(activity);
		final Account[] accounts = googleAccountManager.getAccounts();

		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle(activity.getString(R.string.login_with_your_a_google_account));

		final int size = accounts.length;

		if (size == 0)
		{
			listener.onAccountNotFound();
			return;
		}

		String[] names = new String[size];

		for (int i = 0; i < size; i++)
		{
			names[i] = accounts[i].name;
		}

		builder.setItems(names, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which)
			{
				currAccount = accounts[which];
				getAuthToken();

			}
		});

		builder.show();
	}

	public void invalidate(final Activity activity, String authToken, String email, OnChooseAccountEvent listener)
	{
		this.listener = listener;
		this.activity = activity;

		final GoogleAccountManager googleAccountManager = new GoogleAccountManager(activity);
		final Account[] accounts = googleAccountManager.getAccounts();

		Account currAccount = null;

		for (Account account : accounts)
		{
			if (account.name.equalsIgnoreCase(email))
			{
				currAccount = account;
				break;
			}
		}

		if (currAccount != null)
		{
			googleAccountManager.invalidateAuthToken(authToken);
			this.currAccount = currAccount;
			getAuthToken();
		} else
		{
			chooseAccount(activity, listener);
		}

	}

	public void getAuthToken()
	{
		final GoogleAccountManager googleAccountManager = new GoogleAccountManager(activity);
		googleAccountManager.getAccountManager().getAuthToken(currAccount, Constants.AUTH_TOKEN_TYPE, null, activity,
				GoogleAccountClient.this, null);
	}

}
