package com.deedle.utils;

import java.util.Random;

import android.content.Context;

import com.deedle.R;

public class BackgroundGenerator
{
	public static int getDeedBackground(Context context)
	{
		int resBg = 0;

		Random random = new Random();

		int nextInt = random.nextInt(3);

		if(nextInt == 0)
		{
			resBg = R.drawable.deed_bg_1;
		}else if(nextInt == 1)
		{
			resBg = R.drawable.deed_bg_2;
		}else
		{
			resBg = R.drawable.deed_bg_3;
		}

		return resBg;
	}
}
