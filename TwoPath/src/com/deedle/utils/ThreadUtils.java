package com.deedle.utils;

import com.dd.utils.log.L;

public class ThreadUtils
{
	public static void wait(Object obj, int milliseconds)
	{
		try
		{
			obj.wait(milliseconds);
		} catch (InterruptedException e1)
		{
			L.e(e1.toString());
		}
	}
}
