package com.deedle.utils;

import android.app.Activity;
import android.content.DialogInterface.OnCancelListener;
import android.location.LocationListener;
import android.os.Handler;

import com.dd.ui.DialogView;
import com.deedle.R;

public class LocationHelper
{
	private static final int DELAY_MILLIS = 1000;
	private static final int CHECK_LOCATION_TIMEOUT = 10;

	private int checkLocationTimer = CHECK_LOCATION_TIMEOUT;

	private final Activity activity;
	private final LocationListener listener;
	private final DialogView dialog = new DialogView();
	private final Handler timerHandler = new Handler();
	private final Runnable timerRunnable = new Runnable() {

		@Override
		public void run()
		{
			if (checkLocationTimer == 0)
			{
				checkLocationTimer = CHECK_LOCATION_TIMEOUT;
				timerHandler.removeCallbacks(timerRunnable);
				LocationUtils.stop(activity, listener);
				checkGPSLocation();
				dialog.closeProgress();
			} else
			{
				checkLocationTimer--;
				activity.runOnUiThread(changeDialogTextRunnable);
				timerHandler.postDelayed(this, DELAY_MILLIS);
			}
		}
	};

	private final Runnable changeDialogTextRunnable = new Runnable() {

		@Override
		public void run()
		{
			String message = activity.getString(R.string.determining_your_location) + " " + checkLocationTimer;
			dialog.showProgressDialog(activity, message);
		}
	};

	public LocationHelper(Activity activity, LocationListener listener, OnCancelListener onCancelListener)
	{
		this.activity = activity;
		this.listener = listener;
		dialog.setOnCancelListener(onCancelListener);
	}

	public void checkNetworkLocation()
	{
		dialog.showProgressDialog(activity, activity.getString(R.string.determining_your_location));
		LocationUtils.getNetworkAddress(activity, listener);
		timerHandler.postDelayed(timerRunnable, DELAY_MILLIS);
	}

	public void stopCheckingLocation()
	{
		timerHandler.removeCallbacks(timerRunnable);
		LocationUtils.stop(activity, listener);
		dialog.closeProgress();
	}

	public void checkGPSLocation()
	{
		if (LocationUtils.isGpsOn(activity))
		{
			LocationUtils.getGPSAddress(activity, listener);
			dialog.showProgressDialog(activity, activity.getString(R.string.determining_your_location));
		} else
		{
			LocationUtils.showGPSSettingsDialog(activity, activity.getString(R.string.please_turn_on_gps_to_determine_your_location));
		}
	}
}
