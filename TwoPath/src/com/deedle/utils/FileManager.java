package com.deedle.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import android.content.Context;
import android.graphics.Bitmap;

import com.dd.utils.log.L;
import com.deedle.data.Constants;

public class FileManager
{

	private final Context context;

	public FileManager(Context context)
	{
		this.context = context;

		createAllFolders();
	}

	public void createAllFolders()
	{
		File file = new File(context.getExternalCacheDir(), Constants.Cache.FOLDER_SHARE);
		file.mkdirs();
	}

	public File getDeedPhoto()
	{
		File file = new File(context.getExternalCacheDir(), Constants.Cache.ADD_DEED_PHOTO);
		return file;
	}

	public File getDeedPhoto(String id, int size)
	{
		File dir = new File(context.getExternalCacheDir(), Constants.Cache.FOLDER_SHARE);
		File file = new File(dir, size + id);
		return file;
	}

	public File getUserPhoto()
	{
		File file = new File(context.getExternalCacheDir(), Constants.Cache.USER_PHOTO);
		return file;
	}

	public void clearCacheFolder()
	{
		File dir = new File(context.getExternalCacheDir(), Constants.Cache.FOLDER_SHARE);
		clearFolder(dir);
	}

	public static void clearFolder(File folder)
	{
		File[] files = folder.listFiles();
		if (files != null)
		{
			for (File f : files)
			{
				if (f.isDirectory())
				{
					clearFolder(f);
				} else
				{
					f.delete();
				}
			}
		}
	}

	public static void saveImage(File imagePath, Bitmap bitmap)
	{
		if (bitmap == null)
		{
			L.e("Image was't save: " + imagePath);
			return;
		}

		try
		{
			FileOutputStream out = new FileOutputStream(imagePath);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
		} catch (FileNotFoundException e)
		{
			L.e(e.toString());
		}
	}
}
