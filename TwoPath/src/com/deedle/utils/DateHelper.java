package com.deedle.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateHelper
{
	public static String getDateFrom(long dateMillis)
	{
		SimpleDateFormat dateFormatt = new SimpleDateFormat("yyyy MMM dd HH:mm");

		Calendar currDate = Calendar.getInstance();

		Calendar deedDate = Calendar.getInstance();
		deedDate.setTimeInMillis(dateMillis);

		String result = dateFormatt.format(deedDate.getTime());

		boolean yearMatch = currDate.get(Calendar.YEAR) == deedDate.get(Calendar.YEAR);
		boolean monthMatch = currDate.get(Calendar.MONTH) == deedDate.get(Calendar.MONTH);
		boolean dayMatch = currDate.get(Calendar.DAY_OF_MONTH) == deedDate.get(Calendar.DAY_OF_MONTH);

		if (yearMatch && monthMatch && dayMatch)
		{
			int hour = currDate.get(Calendar.HOUR_OF_DAY) - deedDate.get(Calendar.HOUR_OF_DAY);

			if (hour == 0)
			{
				int minutes = currDate.get(Calendar.MINUTE) - deedDate.get(Calendar.MINUTE);

				if (minutes <= 1)
				{
					result = minutes + " minute ago";
				} else
				{
					result = minutes + " minutes ago";
				}
			} else if (hour == 1)
			{
				result = hour + " hour ago";
			} else
			{
				result = hour + " hours ago";
			}
		}

		return result;
	}
}
