package com.deedle.utils;

import android.view.MotionEvent;
import android.view.View;

public class SwipeUtils implements View.OnTouchListener
{
	public interface OnSwipeListener
	{
		public void onRightToLeftSwipe();

		public void onLeftToRightSwipe();

		public void onTopToBottomSwipe();

		public void onBottomToTopSwipe();
	}

	private int minDistance = 100;
	private float downX, downY, upX, upY;
	private final OnSwipeListener listener;

	public SwipeUtils(OnSwipeListener listener)
	{
		this.listener = listener;
	}

	public SwipeUtils(OnSwipeListener listener, int minDistance)
	{
		this.listener = listener;
		this.minDistance = minDistance;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		switch (event.getAction())
		{
			case MotionEvent.ACTION_DOWN:
			{
				downX = event.getX();
				downY = event.getY();

				//				return true;
			}
			case MotionEvent.ACTION_UP:
			{
				upX = event.getX();
				upY = event.getY();

				float deltaX = downX - upX;
				//				float deltaY = downY - upY;

				// swipe horizontal?
				if (Math.abs(deltaX) > minDistance)
				{
					// left or right
					if (deltaX < 0)
					{
						listener.onLeftToRightSwipe();
						return true;
					}
					if (deltaX > 0)
					{
						listener.onRightToLeftSwipe();
						return true;
					}
				} else
				{
					return false; // We don't consume the event
				}

				// swipe vertical?
				//				if (Math.abs(deltaY) > minDistance)
				//				{
				//					// top or down
				//					if (deltaY < 0)
				//					{
				//						listener.onTopToBottomSwipe();
				//						return true;
				//					}
				//					if (deltaY > 0)
				//					{
				//						listener.onBottomToTopSwipe();
				//						return true;
				//					}
				//				} else
				//				{
				//					return false; // We don't consume the event
				//				}
				//
				//				return true;
			}
		}
		return false;
	}

}
