package com.deedle.utils;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;

import com.dd.utils.log.L;

public class LocationUtils
{
	public static Address getAddress(Context context)
	{
		Address address = null;

		Location location = getLastKnownLocation(context);

		if (location == null)
		{
			return address;
		}

		double lat = location.getLatitude();
		double lng = location.getLongitude();

		Geocoder geocoder = new Geocoder(context, Locale.getDefault());
		try
		{
			List<Address> addressList = geocoder.getFromLocation(lat, lng, 1);

			if (addressList.size() > 0)
			{
				address = addressList.get(0);
			}

		} catch (IOException e)
		{
			L.e(e.toString());
		}

		return address;
	}

	private static Location getLastKnownLocation(Context context)
	{
		LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		Location location = null;

		List<String> providers = manager.getProviders(true);

		for (String provider : providers)
		{
			location = manager.getLastKnownLocation(provider);

			if (location != null)
			{
				return location;
			}
		}

		return location;
	}

	public static Address getAddress(Context context, Location location)
	{
		Address address = null;

		if (location == null)
		{
			return address;
		}

		double lat = location.getLatitude();
		double lng = location.getLongitude();

		Geocoder geocoder = new Geocoder(context, Locale.getDefault());
		try
		{
			List<Address> addressList = geocoder.getFromLocation(lat, lng, 1);

			if (addressList.size() > 0)
			{
				address = addressList.get(0);
			}

		} catch (IOException e)
		{
			L.e(e.toString());
		}

		return address;
	}

	public static boolean isGpsOn(Context context)
	{
		boolean result = false;
		LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

		if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
		{
			result = true;
		}

		return result;
	}

	public static void showGPSSettingsDialog(final Context context, String message)
	{
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message).setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(final DialogInterface dialog, final int id)
			{
				context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
			}
		}).setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(final DialogInterface dialog, final int id)
			{
				dialog.cancel();
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	public static void stop(Context context, LocationListener listener)
	{
		LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		manager.removeUpdates(listener);
	}

	public static void getNetworkAddress(Context context, LocationListener listener)
	{
		LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		manager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, listener, null);
	}

	public static void getGPSAddress(Context context, LocationListener listener)
	{
		LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		manager.requestSingleUpdate(LocationManager.GPS_PROVIDER, listener, null);
	}
}
