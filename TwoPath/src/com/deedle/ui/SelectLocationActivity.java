package com.deedle.ui;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.dd.utils.log.L;
import com.deedle.R;
import com.deedle.api.v2.deed.DeedData;
import com.deedle.api.v2.user.UserData;
import com.deedle.data.DataHolder;
import com.deedle.ui.view.HeaderView;
import com.deedle.utils.LocationHelper;
import com.deedle.utils.LocationUtils;

public class SelectLocationActivity extends BaseActivity implements OnClickListener, LocationListener, OnCheckedChangeListener, OnCancelListener
{

	private final UserData userData = DataHolder.INSTANCE.getUserData();
	private LocationHelper locationHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_location);

		if (userData == null)
		{
			ActivityManager.startLoginActivity(this);
			finish();
			return;
		}

		locationHelper = new LocationHelper(this, this, this);

		initHeader();
		initView();
	}

	@Override
	public void onLocationChanged(Location location)
	{
		if(location == null)
		{
			location = new Location(LocationManager.NETWORK_PROVIDER);
		}

		L.i("Location: " + location.getLatitude() + " : " + location.getLongitude());

		locationHelper.stopCheckingLocation();

		locationHelper.stopCheckingLocation();
		DeedData deedData = DataHolder.INSTANCE.getDeedData();

		if (location != null)
		{
			deedData.setLatitude(location.getLatitude());
			deedData.setLongtitude(location.getLongitude());
		}

		Address address = LocationUtils.getAddress(this, location);

		String country = null;
		String city = null;
		String adminArea = null;

		if (address != null)
		{
			country = address.getCountryName();
			city = address.getLocality();
			adminArea = address.getAdminArea();
		}

		StringBuilder result = new StringBuilder();

		if (country != null)
		{
			deedData.setCountry(country);
			result.append(country);
		}

		if (city != null)
		{
			deedData.setCity(city);
			result.append(", ");
			result.append(city);
		} else if (adminArea != null)
		{
			deedData.setCity(adminArea);
			result.append(", ");
			result.append(adminArea);
		}

		updateYourLocationText(result.toString());
	}

	@Override
	public void onProviderDisabled(String provider)
	{
		// unused
	}

	@Override
	public void onProviderEnabled(String provider)
	{
		// unused
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras)
	{
		// unused
	}

	@Override
	public void onClick(View v)
	{
		int id = v.getId();

		if (id == R.id.imgLogo)
		{
			finish();
		}
	}

	@Override
	public void onCancel(DialogInterface dialog)
	{
		locationHelper.stopCheckingLocation();
	}

	@Override
	public void onCheckedChanged(RadioGroup view, int id)
	{
		DeedData deed = DataHolder.INSTANCE.getDeedData();

		if (id == R.id.rbYourCurrLocation)
		{
			determineLocation();
		} else if (id == R.id.rbMyCity)
		{
			deed.setCity(userData.getCity());
			deed.setCountry(userData.getCountry());
		} else if (id == R.id.rbHideLocation)
		{
			deed.setCity(null);
			deed.setCountry(null);
		}

	}

	private void determineLocation()
	{
		locationHelper.checkNetworkLocation();
	}

	private void updateYourLocationText(String text)
	{
		RadioButton rbYourCurrLocation = (RadioButton) findViewById(R.id.rbYourCurrLocation);
		rbYourCurrLocation.setText(prepareText(getString(R.string.your_current_location), text));
	}

	private void initView()
	{
		RadioGroup rgLocation = (RadioGroup) findViewById(R.id.rgLocation);
		rgLocation.setOnCheckedChangeListener(this);

		updateYourLocationText(getString(R.string.undefined));

		String city = userData.getCity();
		String text = getString(R.string.undefined);

		if (city != null)
		{
			text = city;
		}

		RadioButton rbMyCity = (RadioButton) findViewById(R.id.rbMyCity);
		rbMyCity.setText(prepareText(getString(R.string.my_city), text));

		RadioButton rbHideLocation = (RadioButton) findViewById(R.id.rbHideLocation);
		rbHideLocation.setText(prepareText(getString(R.string.hide_location), getString(R.string.do_not_show_location_on_post)));
	}

	private SpannableString prepareText(String title, String description)
	{
		int titleColor = getResources().getColor(R.color.title_text);
		int descrColor = getResources().getColor(R.color.descr_text);

		String text = title + "\n" + description;

		SpannableString span = new SpannableString(title + "\n" + description);
		span.setSpan(new ForegroundColorSpan(titleColor), 0, title.length(), 0);
		span.setSpan(new ForegroundColorSpan(descrColor), title.length(), text.length(), 0);
		span.setSpan(new RelativeSizeSpan(0.8f), title.length(), text.length(), 0);

		return span;
	}

	private void initHeader()
	{
		HeaderView header = new HeaderView(findViewById(R.id.headerView));
		header.clean();
		header.createLogo(this);
		header.createTitle(getString(R.string.select_location));
	}

}
