package com.deedle.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.dd.utils.Utils;
import com.deedle.R;

public class NoInternetActivity extends Activity implements OnClickListener
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.no_internet);

		initView();
	}

	private void initView()
	{
		Button btnCheckInternet = (Button) findViewById(R.id.btnCheckInternet);
		btnCheckInternet.setOnClickListener(this);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK && !Utils.Network.isOn(this))
		{
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(View v)
	{
		int id = v.getId();

		if (R.id.btnCheckInternet == id)
		{
			openNetworkSettingsIntent();
		}
	}

	private void openNetworkSettingsIntent()
	{
		startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));

	}
}
