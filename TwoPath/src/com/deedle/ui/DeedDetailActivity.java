package com.deedle.ui;

import java.io.File;
import java.util.List;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.deedle.R;
import com.deedle.adapter.DeedDetailAdapter;
import com.deedle.api.v2.deed.DeedClient;
import com.deedle.api.v2.deed.DeedData;
import com.deedle.api.v2.user.UserData;
import com.deedle.data.Constants;
import com.deedle.data.DataHolder;
import com.deedle.loader.ImageLoader;
import com.deedle.ui.view.HeaderView;
import com.deedle.ui.view.VoteBtnFactory;
import com.deedle.utils.BackgroundGenerator;
import com.deedle.utils.FileManager;
import com.viewpagerindicator.TitlePageIndicator;

public class DeedDetailActivity extends BaseActivity implements OnClickListener
{
	private static final String[] PAGE_TITLES = { "INFORMATION", "COMMENTS" };
	private final UserData userData = DataHolder.INSTANCE.getUserData();
	private final DeedData deedData = DataHolder.INSTANCE.getDeedData();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.deed_detail);

		initHeader();
		initView();
		initVoteView();
	}

	@Override
	public void onClick(View view)
	{
		int id = view.getId();

		if (id == R.id.imgLogo)
		{
			finish();
		} else if (id == R.id.btnLike)
		{
			startLikeTask();
		} else if (id == R.id.btnDislike)
		{
			startDislikeTask();
		}
	}

	private void startLikeTask()
	{
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected void onPreExecute()
			{
				deedData.setLike(deedData.getLike() + 1);
				initVotedButtons();
			};

			@Override
			protected Void doInBackground(Void... params)
			{
				DeedClient client = new DeedClient();
				client.like(userData.getEmail(), deedData.getPhotoId());
				userData.getVotedList().add(deedData.getPhotoId());

				return null;
			}

		}.execute();
	}

	private void startDislikeTask()
	{
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected void onPreExecute()
			{
				deedData.setUnlike(deedData.getDislike() + 1);
				initVotedButtons();
			};

			@Override
			protected Void doInBackground(Void... params)
			{
				DeedClient client = new DeedClient();
				client.dislike(userData.getEmail(), deedData.getPhotoId());
				userData.getVotedList().add(deedData.getPhotoId());

				return null;
			}

		}.execute();
	}

	private void initHeader()
	{
		HeaderView header = new HeaderView(findViewById(R.id.header));
		header.clean();
		header.createLogo(this);
	}

	private void initView()
	{
		loadDeedImage(deedData);

		ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);

		TitlePageIndicator titleIndicator = (TitlePageIndicator) findViewById(R.id.titlesView);
		DeedDetailAdapter adapter = new DeedDetailAdapter(this, PAGE_TITLES, deedData, userData);

		viewPager.setAdapter(adapter);

		titleIndicator.setViewPager(viewPager);
		titleIndicator.setCurrentItem(0);

	}

	private void initVoteView()
	{

		if (userVoted())
		{
			initVotedButtons();
		} else
		{
			initUnvotedButtons();
		}

	}

	private void initUnvotedButtons()
	{
		LinearLayout voteBtnHolder = (LinearLayout) findViewById(R.id.voteBtnHolder);
		voteBtnHolder.removeAllViews();

		LayoutParams params = new LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		params.setMargins(12, 0, 0, 0);

		Button likeBtn = VoteBtnFactory.createUnvoteLikeButton(this, Integer.toString(deedData.getLike()), this);
		Button dislikeBtn = VoteBtnFactory.createUnvoteDislikeButton(this, Integer.toString(deedData.getDislike()), this);
		dislikeBtn.setLayoutParams(params);
		voteBtnHolder.addView(likeBtn);
		voteBtnHolder.addView(dislikeBtn);
	}

	private void initVotedButtons()
	{
		LinearLayout voteBtnHolder = (LinearLayout) findViewById(R.id.voteBtnHolder);
		voteBtnHolder.removeAllViews();

		LayoutParams params = new LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		params.setMargins(12, 0, 0, 0);

		Button likeBtn = VoteBtnFactory.createVoteLikeButton(this, Integer.toString(deedData.getLike()));
		Button dislikeBtn = VoteBtnFactory.createVoteDislikeButton(this, Integer.toString(deedData.getDislike()));
		dislikeBtn.setLayoutParams(params);
		voteBtnHolder.addView(likeBtn);
		voteBtnHolder.addView(dislikeBtn);
	}

	private boolean userVoted()
	{
		boolean result = false;
		List<String> votedList = userData.getVotedList();
		if (votedList.contains(deedData.getPhotoId()))
		{
			result = true;
		}

		return result;
	}

	protected void loadDeedImage(final DeedData deedData)
	{
		ImageView imageView = (ImageView) findViewById(R.id.imgPhoto);
		imageView.setImageResource(BackgroundGenerator.getDeedBackground(this));
		FileManager fileManager = new FileManager(this);
		File imagePath = fileManager.getDeedPhoto(deedData.getPhotoId(), Constants.DEED_IMAGE_BIG);
		ImageLoader loader = new ImageLoader();
		loader.load(imageView, imagePath, deedData.getPhotoId(), Constants.DEED_IMAGE_BIG);

	}

}
