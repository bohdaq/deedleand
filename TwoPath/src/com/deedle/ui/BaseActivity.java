package com.deedle.ui;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.AsyncTask;

import com.dd.utils.log.L;
import com.google.android.maps.MapActivity;

public class BaseActivity extends MapActivity
{
	@Override
	protected void onResume()
	{
		IntentFilter filter = new IntentFilter();
		filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

		registerReceiver(receiver, filter);

		super.onResume();
	}

	@Override
	protected void onPause()
	{
		unregisterReceiver(receiver);
		super.onPause();
	}

	@Override
	protected boolean isRouteDisplayed()
	{
		return false;
	}

	private final BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent)
		{
			checkNetwork();
		}
	};

	private void checkNetwork()
	{
		new AsyncTask<Void, Void, Boolean>() {

			@Override
			protected Boolean doInBackground(Void... params)
			{
				boolean isNetworkAvailable = false;

				try
				{
					URL url = new URL("http://google.com");
					HttpURLConnection connection = (HttpURLConnection) url.openConnection();
					connection.connect();
					int code = connection.getResponseCode();

					if (code == 200)
					{
						isNetworkAvailable = true;
					}

				} catch (IOException e)
				{
					L.e(e.toString());
				}

				return isNetworkAvailable;
			}

			@Override
			protected void onPostExecute(Boolean isNetworkAvailable)
			{
				if(!isNetworkAvailable)
				{
					startNoInternetActiviy();
				}
			};

		}.execute();
	}

	private void startNoInternetActiviy()
	{
		Intent intent = new Intent(this, NoInternetActivity.class);
		startActivity(intent);
	}
}
