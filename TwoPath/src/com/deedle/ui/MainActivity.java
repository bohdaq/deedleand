package com.deedle.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.deedle.R;
import com.deedle.api.v2.deed.FilterData;
import com.deedle.api.v2.deed.FilterData.Rating;
import com.deedle.api.v2.deed.FilterData.Type;
import com.deedle.api.v2.user.UserData;
import com.deedle.data.DataHolder;
import com.deedle.ui.view.HeaderView;
import com.deedle.ui.view.MenuView;
import com.deedle.ui.view.MenuView.MenuItemClickListener;
import com.deedle.ui.view.SliderView;
import com.deedle.ui.view.main.BaseView;
import com.deedle.ui.view.main.PeopleView;
import com.deedle.ui.view.main.UserDetailView;
import com.deedle.ui.view.main.ViewId;
import com.deedle.ui.view.main.ViewManager;
import com.deedle.ui.view.main.timeline.TimelineGridView;
import com.deedle.ui.view.main.timeline.TimelineListView;
import com.deedle.ui.view.main.timeline.TimelineMapView;
import com.deedle.ui.view.main.timeline.TimelineView;
import com.deedle.utils.FileManager;
import com.deedle.utils.SwipeUtils;
import com.deedle.utils.ToastUtils;

public class MainActivity extends BaseActivity implements SwipeUtils.OnSwipeListener, MenuItemClickListener, OnClickListener
{
	private final UserData userData = DataHolder.INSTANCE.getUserData();
	private final FilterData filterData = DataHolder.INSTANCE.getFilterData();
	private ViewManager viewManager;
	private HeaderView headerView;
	private SliderView slideView;
	private MenuView menuView;
	private SwipeUtils swipeDetector;
	private TimelineView timelineView;
	private ViewId currTimelineViewId;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		if (userData == null)
		{
			ActivityManager.startLoginActivity(this);
			finish();
			return;
		}

		initDefaultFilter();
		initView();
	}

	@Override
	public void onRightToLeftSwipe()
	{
		slideView.closeMenu();
		menuView.setClosed(true);
		createInitialHeader();
	}

	@Override
	public void onLeftToRightSwipe()
	{
		if (ViewId.TIMELINE_MAP == currTimelineViewId && viewManager.isActive(ViewId.TIMELINE_MAP))
		{
			return;
		}

		slideView.openMenu();
		menuView.setClosed(false);
		headerView.clean();
		headerView.createLogo();
	}

	@Override
	public void onTopToBottomSwipe()
	{

	}

	@Override
	public void onBottomToTopSwipe()
	{

	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent e)
	{
		boolean consumeEvent = swipeDetector.onTouch(slideView, e);

		if (!consumeEvent)
		{
			super.dispatchTouchEvent(e);
		}
		return consumeEvent;
	}

	@Override
	public void onUserDetailSelected()
	{
		viewManager.activate(ViewId.USER_DETAIL);
		createInitialHeader();
	}

	@Override
	public void onDeedFeedSelected()
	{
		viewManager.activate(currTimelineViewId);
	}

	@Override
	public void onPeopleSelected()
	{
		viewManager.activate(ViewId.PEOPLE);
	}

	@Override
	public void onClick(View v)
	{
		int id = v.getId();

		if (id == R.id.imgLogo)
		{
			handleOnLogoClick();
		} else if (id == R.id.imgRefresh)
		{
			handleOnRefreshClick();
		} else if (id == R.id.imgAddNewDeed)
		{
			handleAddDeedClick();
		}

	}

	@Override
	public void onGeneralSelected()
	{
		filterData.setType(Type.ALL);
		handleOnRefreshClick();
	}

	@Override
	public void onFollowingSelected()
	{
		ToastUtils.showNotImplemented(this);
		//		filterData.setType(Type.FOLLOWING);
		//		handleOnRefreshClick();
	}

	@Override
	public void onAllSelected()
	{
		filterData.setRating(Rating.ALL);
		handleOnRefreshClick();
	}

	@Override
	public void onGoodSelected()
	{
		filterData.setRating(Rating.LIKE);
		handleOnRefreshClick();
	}

	@Override
	public void onBadSelected()
	{
		filterData.setRating(Rating.UNLIKE);
		handleOnRefreshClick();
	}

	@Override
	public void onGridSelected()
	{
		timelineView.clean();

		currTimelineViewId = ViewId.TIMELINE_GRID;
		timelineView = (TimelineView) viewManager.getView(ViewId.TIMELINE_GRID);
		handleOnRefreshClick();
		onDeedFeedSelected();

	}

	@Override
	public void onListSelected()
	{
		timelineView.clean();

		currTimelineViewId = ViewId.TIMELINE_LIST;
		timelineView = (TimelineView) viewManager.getView(ViewId.TIMELINE_LIST);
		handleOnRefreshClick();
		onDeedFeedSelected();
	}

	@Override
	public void onMapSelected()
	{
		timelineView.clean();

		currTimelineViewId = ViewId.TIMELINE_MAP;
		timelineView = (TimelineView) viewManager.getView(ViewId.TIMELINE_MAP);
		handleOnRefreshClick();
		onDeedFeedSelected();
	}

	private void handleAddDeedClick()
	{
		Intent intent = new Intent(this, AddDeedActivity.class);
		startActivity(intent);
	}

	private void handleOnRefreshClick()
	{
		timelineView.refreshTimeline();
	}

	private void handleOnLogoClick()
	{
		if (slideView.isOpen())
		{
			slideView.closeMenu();
			menuView.setClosed(true);
			createInitialHeader();
		} else
		{
			slideView.openMenu();
			menuView.setClosed(false);
			headerView.clean();
			headerView.createLogo();
		}

	}

	private void createInitialHeader()
	{
		headerView.clean();
		headerView.createLogo(this);
		headerView.createRefreshButton(this);
		headerView.createAddNewDeedButton(this);
	}

	private void initDefaultFilter()
	{
		filterData.setCount(100);
		filterData.setEmail(userData.getEmail());
		filterData.setSkip(0);
		filterData.setRating(Rating.ALL);
		filterData.setType(Type.ALL);
		// filterData.setCity("Lviv");
		// filterData.setCountry("Ukraine");

	}

	private void initView()
	{
		headerView = new HeaderView(this);
		createInitialHeader();

		swipeDetector = new SwipeUtils(this);

		FileManager fileManger = new FileManager(this);

		String name = userData.getName();
		Bitmap bitmap = BitmapFactory.decodeFile(fileManger.getUserPhoto().getAbsolutePath());

		menuView = new MenuView(this, this);
		menuView.initUserInfo(name, bitmap);
		viewManager = new ViewManager(this);

		BaseView peopleView = new PeopleView(this);
		BaseView userDetailView = new UserDetailView(this);
		userDetailView.setHeaderView(headerView);

		TimelineGridView timelineGridView = new TimelineGridView(this);
		timelineGridView.setHeaderView(headerView);

		TimelineListView timelineListView = new TimelineListView(this);
		timelineListView.setHeaderView(headerView);

		TimelineMapView mapView = new TimelineMapView(this);
		mapView.setHeaderView(headerView);

		timelineView = timelineGridView;

		viewManager.add(peopleView);
		viewManager.add(userDetailView);
		viewManager.add(timelineGridView);
		viewManager.add(timelineListView);
		viewManager.add(mapView);

		slideView = new SliderView(this);
		slideView.addView(menuView.getView());
		slideView.addView(viewManager.getView());

		LinearLayout rootView = (LinearLayout) findViewById(R.id.rootView);
		rootView.addView(headerView.getView());
		rootView.addView(slideView);

		currTimelineViewId = ViewId.TIMELINE_GRID;
		timelineView = (TimelineView) viewManager.getView(ViewId.TIMELINE_GRID);
		handleOnRefreshClick();
		onDeedFeedSelected();

	}


}