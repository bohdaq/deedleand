package com.deedle.ui;

import android.content.Context;
import android.content.Intent;

public class ActivityManager
{
	public static void startLoginActivity(Context context)
	{
		Intent intent = new Intent(context, LoginActivity.class);
		context.startActivity(intent);
	}
}
