package com.deedle.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.utils.Utils;
import com.deedle.R;
import com.deedle.api.account.GoogleAccount;
import com.deedle.api.account.GoogleAccountClient;
import com.deedle.api.drive.DriveClient;
import com.deedle.api.v2.Result;
import com.deedle.api.v2.user.UserClient;
import com.deedle.api.v2.user.UserData;
import com.deedle.data.Constants;
import com.deedle.data.DataHolder;
import com.deedle.data.Pref;
import com.deedle.events.OnChooseAccountEvent;
import com.deedle.utils.FileManager;
import com.google.android.maps.MapActivity;

public class LoginActivity extends BaseActivity implements android.view.View.OnClickListener, OnChooseAccountEvent
{
	private Animation refreshAnimation;
	private final GoogleAccountClient client = new GoogleAccountClient();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		hideLoadingText();
		initView();

	}

	@Override
	protected void onResume()
	{
		super.onResume();

		GoogleAccount googleAccount = Pref.getGoogleAccount(LoginActivity.this);

		if (Utils.Network.isOn(LoginActivity.this) && GoogleAccount.isValid(googleAccount))
		{
			showLoadingText();
			startLoadAnimation();
			updateLoadingText(R.string.checking_connection);
			client.invalidate(LoginActivity.this, googleAccount.getAuthtoken(), googleAccount.getEmail(), this); // onChooseAccount
		}
	}

	@Override
	public void onClick(View v)
	{
		int id = v.getId();

		if (id == R.id.btnLogin)
		{
			showChooseAccountDialog();
		}

	}

	@Override
	public void onChooseAccount(final GoogleAccount result)
	{
		showLoadingText();
		startLoadAnimation();
		Pref.clearGoogleAccount(LoginActivity.this);
		Pref.setGoogleAccount(LoginActivity.this, result);

		startLoginTask();
	}

	@Override
	public void onAccountNotFound()
	{
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(LoginActivity.this);
		alertBuilder.setTitle(R.string.oops);
		alertBuilder.setMessage(R.string.cant_find_your_google_account);
		alertBuilder.setPositiveButton(R.string.yes, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				startAccountSettingsScreen();
			}
		});

		alertBuilder.setNegativeButton(R.string.no, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				finish();
			}
		});

		alertBuilder.show();

	}

	private void hideLoadingText()
	{
		ImageView imgPreloader = (ImageView) findViewById(R.id.imgPreloader);
		imgPreloader.setVisibility(View.INVISIBLE);

		TextView txtLoad = (TextView) findViewById(R.id.txtLoad);
		txtLoad.setVisibility(View.INVISIBLE);
	}

	private void showLoadingText()
	{
		ImageView imgPreloader = (ImageView) findViewById(R.id.imgPreloader);
		imgPreloader.setVisibility(View.VISIBLE);

		TextView txtLoad = (TextView) findViewById(R.id.txtLoad);
		txtLoad.setVisibility(View.VISIBLE);
	}

	private void updateLoadingText(int resid)
	{
		TextView txtLoad = (TextView) findViewById(R.id.txtLoad);
		txtLoad.setText(resid);
	}

	private void initView()
	{
		Button btnLogin = (Button) findViewById(R.id.btnLogin);
		btnLogin.setOnClickListener(this);
	}

	private void startLoadAnimation()
	{
		refreshAnimation = AnimationUtils.loadAnimation(this, R.anim.refresh);
		refreshAnimation.setRepeatCount(Animation.INFINITE);

		ImageView imgPreloader = (ImageView) findViewById(R.id.imgPreloader);
		imgPreloader.startAnimation(refreshAnimation);
	}

	private void stopLoadAnimation()
	{
		refreshAnimation.cancel();
	}

	private void showChooseAccountDialog()
	{
		if (Utils.Network.isOn(LoginActivity.this))
		{
			client.chooseAccount(LoginActivity.this, this); // onChooseAccount
		} else
		{
			Toast.makeText(LoginActivity.this, R.string.no_internet, Toast.LENGTH_LONG).show();
		}
	}

	private void startLoginTask()
	{
		new AsyncTask<Void, UserData, UserData>() {
			@Override
			protected void onPreExecute()
			{
				updateLoadingText(R.string.performing_login_);
			}

			@Override
			protected UserData doInBackground(Void... params)
			{

				String email = Pref.getGoogleAccount(LoginActivity.this).getEmail();
				UserClient client = new UserClient();
				UserData userData = client.login(email);

				return userData;
			}

			@Override
			protected void onPostExecute(UserData result)
			{
				onLogin(result);
			}
		}.execute();

	}

	private void startAccountSettingsScreen()
	{
		Intent intent = new Intent(android.provider.Settings.ACTION_ADD_ACCOUNT);
		startActivity(intent);
	}

	private void onLogin(UserData result)
	{
		DataHolder.INSTANCE.setUserData(result);

		if (Result.SUCCESS.equals(result.getResult()))
		{
			loadDataFromNetwork(result);

		} else if (Result.USER_NOT_REGISTERED.equals(result.getResult()))
		{
			stopLoadAnimation();
			startUserInfoActivity();
			finish();
		} else
		{
			Toast.makeText(LoginActivity.this, R.string.login_fail, Toast.LENGTH_SHORT).show();
			stopLoadAnimation();
		}

	}

	private void loadDataFromNetwork(final UserData userData)
	{
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected void onPreExecute()
			{
				updateLoadingText(R.string.retrieving_data_);
			};

			@Override
			protected Void doInBackground(Void... params)
			{
				DriveClient client = new DriveClient();

				Bitmap bitmap = client.loadFile(userData.getPhotoId(), Constants.USER_PHOTO);

				FileManager fileManager = new FileManager(LoginActivity.this);

				if (bitmap != null)
				{
					FileManager.saveImage(fileManager.getUserPhoto(), bitmap);
				}

				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				stopLoadAnimation();
				startMainActivity();
				finish();
			};

		}.execute();
	}

	@Override
	protected boolean isRouteDisplayed()
	{
		return false;
	}

	private void startMainActivity()
	{
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
	}

	private void startUserInfoActivity()
	{
		Intent intent = new Intent(this, RegistrationActivity.class);
		startActivity(intent);
	}

}
