package com.deedle.ui;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.commands.BitmapCommands;
import com.dd.ui.DialogView;
import com.deedle.R;
import com.deedle.api.drive.DriveClient;
import com.deedle.api.v2.Result;
import com.deedle.api.v2.deed.DeedClient;
import com.deedle.api.v2.deed.DeedData;
import com.deedle.api.v2.user.UserData;
import com.deedle.data.Constants;
import com.deedle.data.DataHolder;
import com.deedle.data.Pref;
import com.deedle.ui.view.HeaderView;
import com.deedle.utils.FileManager;
import com.google.api.services.drive.model.File;

public class AddDeedActivity extends BaseActivity implements OnClickListener, TextWatcher
{

	private static final int REQ_CODE_PICK_IMAGE = 1000;
	private static final int REQ_CODE_TAKE_IMAGE = 2000;

	private static final int MINIMUM_LETTERS_FOR_POST = 2;

	private final DialogView dialog = new DialogView();
	private final DeedData deedData = DataHolder.INSTANCE.getDeedData();
	private final UserData userData = DataHolder.INSTANCE.getUserData();
	private java.io.File deedPhoto;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_deed);

		if (userData == null)
		{
			ActivityManager.startLoginActivity(this);
			finish();
			return;
		}

		FileManager fileManager = new FileManager(this);
		deedPhoto = fileManager.getDeedPhoto();
		deedPhoto.delete(); // clear previous shared image

		initHeader();
		initView();
		validate();
	}

	@Override
	protected void onResume()
	{
		if (deedData.getCountry() != null && deedData.getCity() != null)
		{
			updateLocationText(deedData.getCountry() + ", " + deedData.getCity());
		} else
		{
			updateLocationText(getString(R.string.select_location));
		}

		super.onResume();
	}

	@Override
	public void onClick(View v)
	{
		int id = v.getId();

		if (id == R.id.imgLogo)
		{
			finish();
		} else if (id == R.id.btnChoosePhoto)
		{
			startGalleryIntent();
		} else if (id == R.id.btnAddPhoto)
		{
			startTakePictureIntent();
		} else if (id == R.id.txtLocation)
		{
			startSelectLocationIntent();
		} else if (id == R.id.btnPost)
		{
			uploadToDriveTask();
		}
	}

	@Override
	public void afterTextChanged(Editable text)
	{
		validate();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
	{
		// unused
	}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
	{
		// unused
	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent intent)
	{
		super.onActivityResult(requestCode, resultCode, intent);

		new AsyncTask<Void, Void, Void>() {
			@Override
			protected void onPreExecute()
			{
				dialog.showProgressDialog(AddDeedActivity.this, AddDeedActivity.this.getString(R.string.loading));
			};

			@Override
			protected Void doInBackground(Void... params)
			{

				Bitmap bitmap = null;

				if (requestCode == REQ_CODE_PICK_IMAGE && resultCode == RESULT_OK)
				{
					Uri selectedImage = intent.getData();
					String[] filePathColumn = { MediaColumns.DATA };

					Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
					cursor.moveToFirst();

					int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
					String filePath = cursor.getString(columnIndex);
					cursor.close();

					bitmap = BitmapCommands.Path.build(filePath, Constants.DEED_IMAGE_BIG, Constants.DEED_IMAGE_BIG);

				} else if (requestCode == REQ_CODE_TAKE_IMAGE && resultCode == RESULT_OK)
				{
					Bundle extras = intent.getExtras();
					bitmap = (Bitmap) extras.get("data");
				}

				if (bitmap != null)
				{
					FileManager.saveImage(deedPhoto, bitmap);
				}

				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				showImage();
				dialog.closeProgress();
			};

		}.execute();
	}

	private void showImage()
	{

		Bitmap bitmap = BitmapFactory.decodeFile((deedPhoto.getAbsolutePath()));
		ImageView imgPhoto = (ImageView) findViewById(R.id.imgPhoto);
		imgPhoto.setImageBitmap(bitmap);

		RelativeLayout holderImage = (RelativeLayout) findViewById(R.id.holderImage);

		if (bitmap != null)
		{
			holderImage.setVisibility(View.VISIBLE);
		}

		validate();
	}

	private void shareTask()
	{
		AsyncTask<Void, Result, Result> task = new AsyncTask<Void, Result, Result>() {
			@Override
			protected void onPreExecute()
			{
				dialog.showProgressDialog(AddDeedActivity.this, "Sharing deed..");
			}

			@Override
			protected Result doInBackground(Void... arg0)
			{

				DeedClient client = new DeedClient();
				Result result = client.newDeed(deedData);

				return result;
			}

			@Override
			protected void onPostExecute(Result result)
			{
				dialog.closeProgress();

				if (Result.SUCCESS == result)
				{

					Toast.makeText(AddDeedActivity.this, "Success", Toast.LENGTH_LONG).show();
					finish();
				} else
				{
					Toast.makeText(AddDeedActivity.this, "Oops we got an error, try again", Toast.LENGTH_LONG).show();
				}

			}

		};

		task.execute();

	}

	private void uploadToDriveTask()
	{
		final String authtoken = Pref.getGoogleAccount(AddDeedActivity.this).getAuthtoken();
		final String folderId = userData.getFolderId();
		final String filePath = deedPhoto.getAbsolutePath();
		deedData.setDate(System.currentTimeMillis());

		final String driveDescription = DriveClient.getDriveFileDescription(deedData.getTitle(), deedData.getDescription(),
				deedData.getDate());

		AsyncTask<Void, File, File> task = new AsyncTask<Void, File, File>() {

			@Override
			protected void onPreExecute()
			{
				dialog.showProgressDialog(AddDeedActivity.this, "Uploading photo to google drive..");
			};

			@Override
			protected File doInBackground(Void... params)
			{
				DriveClient driveCLient = new DriveClient();

				File uploadedFile = driveCLient.uploadFile(authtoken, Constants.Drive.API_KEY, filePath, deedData.getTitle(),
						driveDescription, folderId);

				return uploadedFile;
			}

			@Override
			protected void onPostExecute(File result)
			{
				if (result != null)
				{
					String email = Pref.getGoogleAccount(AddDeedActivity.this).getEmail();

					deedData.setPhotoId(result.getId());
					deedData.setEmail(email);
					deedData.setName(userData.getName());
					deedData.setUserPhotoId(userData.getPhotoId());

					shareTask();

				} else
				{
					Toast.makeText(AddDeedActivity.this, getString(R.string.oops), Toast.LENGTH_LONG).show();
					dialog.closeProgress();
				}

			};
		};

		task.execute();

	}

	private boolean isCameraAvailable()
	{
		boolean result = false;

		PackageManager pm = getPackageManager();

		if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA))
		{
			result = true;
		}

		return result;
	}

	private void startTakePictureIntent()
	{
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(takePictureIntent, REQ_CODE_TAKE_IMAGE);
	}

	private void startGalleryIntent()
	{
		Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(intent, REQ_CODE_PICK_IMAGE);
	}

	private void startSelectLocationIntent()
	{
		Intent intent = new Intent(this, SelectLocationActivity.class);
		startActivity(intent);
	}

	private void initView()
	{
		Button btnPost = (Button) findViewById(R.id.btnPost);
		btnPost.setOnClickListener(this);

		Button btnChoosePhoto = (Button) findViewById(R.id.btnChoosePhoto);
		btnChoosePhoto.setOnClickListener(this);

		Button btnAddPhoto = (Button) findViewById(R.id.btnAddPhoto);
		btnAddPhoto.setOnClickListener(this);
		btnAddPhoto.setEnabled(isCameraAvailable());

		TextView txtLocation = (TextView) findViewById(R.id.txtLocation);
		txtLocation.setOnClickListener(this);

		EditText inputTitle = (EditText) findViewById(R.id.inputTitle);
		inputTitle.addTextChangedListener(this);

		EditText inputDescription = (EditText) findViewById(R.id.inputDescription);
		inputDescription.addTextChangedListener(this);
	}

	private void updateLocationText(String location)
	{
		TextView txtLocation = (TextView) findViewById(R.id.txtLocation);
		txtLocation.setText(location);
	}

	private void validate()
	{
		EditText inputTitle = (EditText) findViewById(R.id.inputTitle);
		EditText inputDescription = (EditText) findViewById(R.id.inputDescription);

		String title = inputTitle.getText().toString().trim();
		String description = inputDescription.getText().toString().trim();

		boolean titleValid = title.length() >= MINIMUM_LETTERS_FOR_POST;
		boolean descriptionValid = description.length() >= MINIMUM_LETTERS_FOR_POST;
		boolean imageValid = deedPhoto.exists();

		if (titleValid && descriptionValid && imageValid)
		{
			deedData.setTitle(title);
			deedData.setDescription(description);

			enablePostButton();
		} else
		{
			disablePostButton();
		}

	}

	private void enablePostButton()
	{
		Button btnPost = (Button) findViewById(R.id.btnPost);
		btnPost.setEnabled(true);
	}

	private void disablePostButton()
	{
		Button btnPost = (Button) findViewById(R.id.btnPost);
		btnPost.setEnabled(false);
	}

	private void initHeader()
	{
		HeaderView header = new HeaderView(findViewById(R.id.headerView));
		header.clean();
		header.createLogo(this);
		header.createTitle(getString(R.string.new_deed));
	}

}
