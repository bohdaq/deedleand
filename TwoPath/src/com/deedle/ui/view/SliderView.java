package com.deedle.ui.view;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class SliderView extends ViewGroup
{

	private boolean isOpen = false;
	private int currentX = 0;
	private int sliderX = 0;

	private Timer timer;

	private void init()
	{
		setChildrenDrawingOrderEnabled(true);
	}

	public SliderView(Context context)
	{

		super(context);

		init();
	}

	public SliderView(Context context, AttributeSet attrs)
	{

		super(context, attrs);

		init();
	}

	public SliderView(Context context, AttributeSet attrs, int defStyle)
	{

		super(context, attrs, defStyle);

		init();
	}

	public boolean isOpen()
	{

		return isOpen;
	}

	public void openMenu()
	{

		if (isOpen)
		{
			return;
		}

		currentX = 0;
		timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				currentX += sliderX / 10;
				if (currentX >= sliderX)
				{
					currentX = sliderX;
					timer.cancel();
				}
				((Activity) getContext()).runOnUiThread(new Runnable() {
					public void run()
					{
						requestLayout();
					}
				});
			}
		}, 16, 16);

		isOpen = true;
	}

	public void closeMenu()
	{

		if (!isOpen)
		{
			return;
		}

		timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run()
			{
				currentX -= sliderX / 10;
				if (currentX <= 0)
				{
					currentX = 0;
					timer.cancel();
				}
				((Activity) getContext()).runOnUiThread(new Runnable() {
					public void run()
					{
						requestLayout();
					}
				});
			}
		}, 16, 16);

		isOpen = false;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{

		int width = MeasureSpec.getSize(widthMeasureSpec);
		int height = MeasureSpec.getSize(heightMeasureSpec);

		sliderX = width * 4 / 5;

		super.setMeasuredDimension(width, height);

		final int count = getChildCount();
		for (int i = 0; i < count; i++)
		{
			View view = getChildAt(i);
			if (i == count - 1)
			{
				measureChild(view, MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), heightMeasureSpec);
			} else
			{
				measureChild(view, MeasureSpec.makeMeasureSpec(sliderX, MeasureSpec.EXACTLY), heightMeasureSpec);
			}
		}
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom)
	{

		int width = right - left;
		int height = bottom - top;

		final int count = getChildCount();
		for (int i = 0; i < count; i++)
		{
			View view = getChildAt(i);
			if (i == count - 1)
			{
				view.layout(currentX, 0, width + currentX, height);
			} else
			{
				view.layout(0, 0, sliderX, height);
			}
		}
	}
}
