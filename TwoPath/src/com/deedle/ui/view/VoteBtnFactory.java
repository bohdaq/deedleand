package com.deedle.ui.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.deedle.R;

public class VoteBtnFactory
{
	public enum Vote
	{
		LIKE,
		DISLIKE;
	}

	public static Button createUnvoteLikeButton(Context context, String text, OnClickListener listener)
	{
		LayoutInflater inflater = LayoutInflater.from(context);
		Button button = (Button) inflater.inflate(R.layout.incl_unvote_like, null);
		button.setText(text);
		button.setOnClickListener(listener);

		return button;

	}

	public static Button createUnvoteDislikeButton(Context context, String text, OnClickListener listener)
	{
		LayoutInflater inflater = LayoutInflater.from(context);
		Button button = (Button) inflater.inflate(R.layout.incl_unvote_dislike, null);
		button.setText(text);
		button.setOnClickListener(listener);

		return button;

	}

	public static Button createVoteDislikeButton(Context context, String text)
	{
		LayoutInflater inflater = LayoutInflater.from(context);
		Button button = (Button) inflater.inflate(R.layout.incl_vote_dislike, null);
		button.setText(text);

		return button;

	}

	public static Button createVoteLikeButton(Context context, String text)
	{
		LayoutInflater inflater = LayoutInflater.from(context);
		Button button = (Button) inflater.inflate(R.layout.incl_vote_like, null);
		button.setText(text);

		return button;

	}
}
