package com.deedle.ui.view.main;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.deedle.R;
import com.deedle.api.v2.user.UserData;
import com.deedle.data.DataHolder;
import com.deedle.utils.FileManager;

public class UserDetailView extends BaseView
{
	private final Activity ac;
	private View view;

	public UserDetailView(Activity ac)
	{
		this.ac = ac;
		initView();

	}

	private void initView()
	{
		FileManager fileManager = new FileManager(ac);

		UserData userData = DataHolder.INSTANCE.getUserData();

		String name = userData.getName();
		String location = userData.getCountry() + " , " + userData.getCity();
		Bitmap bitmap = BitmapFactory.decodeFile(fileManager.getUserPhoto().getAbsolutePath());

		TextView txtName = (TextView) getView().findViewById(R.id.txtName);
		TextView txtLocation = (TextView) getView().findViewById(R.id.txtLocation);
		ImageView imgPhoto = (ImageView)getView().findViewById(R.id.imgPhoto);

		txtName.setText(name);
		txtLocation.setText(location);
		imgPhoto.setImageBitmap(bitmap);
	}

	@Override
	public View getView()
	{
		LayoutInflater inflater = ac.getLayoutInflater();

		if (view == null)
		{
			view = inflater.inflate(R.layout.user_detail, null);
		}

		return view;
	}

	@Override
	public ViewId getId()
	{
		return ViewId.USER_DETAIL;
	}

}
