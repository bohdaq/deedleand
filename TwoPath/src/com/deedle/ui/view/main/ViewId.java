package com.deedle.ui.view.main;

public enum ViewId
{
	USER_DETAIL,
	PEOPLE,
	TIMELINE_GRID,
	TIMELINE_LIST,
	TIMELINE_MAP;
}
