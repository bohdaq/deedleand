package com.deedle.ui.view.main.timeline;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.deedle.R;
import com.deedle.adapter.TimelineGridAdapter;
import com.deedle.api.v2.deed.DeedClient;
import com.deedle.api.v2.deed.DeedData;
import com.deedle.api.v2.deed.FilterData;
import com.deedle.api.v2.user.UserData;
import com.deedle.data.DataHolder;
import com.deedle.ui.DeedDetailActivity;
import com.deedle.ui.view.main.BaseView;
import com.deedle.ui.view.main.ViewId;

public class TimelineGridView extends BaseView implements TimelineView
{
	private final FilterData filterData = DataHolder.INSTANCE.getFilterData();
	private final UserData userData = DataHolder.INSTANCE.getUserData();

	private final Activity ac;
	private View view;

	public TimelineGridView(Activity ac)
	{
		this.ac = ac;
	}

	@Override
	public ViewId getId()
	{
		return ViewId.TIMELINE_GRID;
	}

	@Override
	public View getView()
	{
		LayoutInflater inflater = ac.getLayoutInflater();

		if (view == null)
		{
			view = inflater.inflate(R.layout.timeline_grid, null);
		}

		return view;
	}

	@Override
	public void clean()
	{
		GridView gridView = (GridView) getView().findViewById(R.id.gridView);
		TimelineGridAdapter adapter = (TimelineGridAdapter) gridView.getAdapter();
		adapter.stopTask();
	}

	@Override
	public void refreshTimeline()
	{
		new AsyncTask<Void, Void, List<DeedData>>() {

			@Override
			protected void onPreExecute()
			{
				getHeaderView().startRefreshAnimation();
			};

			@Override
			protected List<DeedData> doInBackground(Void... params)
			{
				DeedClient client = new DeedClient();
				List<DeedData> deedList = client.getDeed(filterData);

				return deedList;
			}

			@Override
			protected void onPostExecute(List<DeedData> deedList)
			{
				getHeaderView().stopRefreshAnimation();
				initView(deedList);

			}
		}.execute();
	}

	private void initView(final List<DeedData> deedList)
	{
		GridView gridView = (GridView) getView().findViewById(R.id.gridView);
		TimelineGridAdapter adapter = (TimelineGridAdapter) gridView.getAdapter();

		if (deedList.size() == 0)
		{
			gridView.setAdapter(null);
		} else
		{

			if (adapter == null)
			{
				adapter = new TimelineGridAdapter(ac, deedList, userData);
				gridView.setAdapter(adapter);
			} else
			{
				adapter.setDeedList(deedList);
				adapter.notifyDataSetChanged();
			}
		}

		if(adapter != null)
		{
			adapter.startTimer();
		}

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
			{
				DeedData deedData = deedList.get(position);
				DataHolder.INSTANCE.setDeedData(deedData);
				startDeedDetailActivity();
			}
		});
	}

	private void startDeedDetailActivity()
	{
		Intent intent = new Intent(ac, DeedDetailActivity.class);
		ac.startActivity(intent);
	}

}
