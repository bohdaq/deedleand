package com.deedle.ui.view.main.timeline;

public interface TimelineView
{
	public void refreshTimeline();
	public void clean();
}
