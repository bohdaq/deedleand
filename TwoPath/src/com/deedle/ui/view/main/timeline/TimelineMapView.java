package com.deedle.ui.view.main.timeline;

import java.io.File;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.dd.commands.BitmapCommands;
import com.deedle.R;
import com.deedle.api.drive.DriveClient;
import com.deedle.api.v2.deed.DeedClient;
import com.deedle.api.v2.deed.DeedData;
import com.deedle.api.v2.deed.FilterData;
import com.deedle.data.Constants;
import com.deedle.data.DataHolder;
import com.deedle.map.CustomItem;
import com.deedle.map.DeedItemOverlay;
import com.deedle.map.OnTapEvent;
import com.deedle.ui.DeedDetailActivity;
import com.deedle.ui.view.main.BaseView;
import com.deedle.ui.view.main.ViewId;
import com.deedle.utils.FileManager;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class TimelineMapView extends BaseView implements TimelineView
{
	private final DriveClient client = new DriveClient();
	private final MapActivity ac;
	private final FileManager fileManager;
	private View view;

	public TimelineMapView(MapActivity ac)
	{
		this.ac = ac;
		this.fileManager = new FileManager(ac);
	}

	@Override
	public ViewId getId()
	{
		return ViewId.TIMELINE_MAP;
	}

	@Override
	public View getView()
	{
		LayoutInflater inflater = ac.getLayoutInflater();

		if (view == null)
		{
			view = inflater.inflate(R.layout.map, null);
		}

		return view;
	}

	@Override
	public void clean()
	{

	}

	@Override
	public void refreshTimeline()
	{
		new AsyncTask<Void, Void, List<DeedData>>() {

			@Override
			protected void onPreExecute()
			{
				getHeaderView().startRefreshAnimation();
			};

			@Override
			protected List<DeedData> doInBackground(Void... params)
			{
				FilterData filter = DataHolder.INSTANCE.getFilterData();

				DeedClient client = new DeedClient();
				List<DeedData> deedList = client.getDeed(filter);

				return deedList;
			}

			@Override
			protected void onPostExecute(List<DeedData> deedList)
			{
				getHeaderView().stopRefreshAnimation();
				loadOverlayList(deedList);
			}
		}.execute();
	}

	private void addDeedToMap(DeedItemOverlay result)
	{
		MapView mapView = (MapView) getView().findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);

		List<Overlay> mapOverlays = mapView.getOverlays();
		mapOverlays.add(result);

	}

	private void loadOverlayList(List<DeedData> deedList)
	{

		for (final DeedData deed : deedList)
		{
			loadOverlay(deed);
		}

	}

	private void loadOverlay(final DeedData deed)
	{
		new AsyncTask<Void, Void, DeedItemOverlay>() {

			@Override
			protected DeedItemOverlay doInBackground(Void... params)
			{

				File imagePath = fileManager.getDeedPhoto(deed.getUserPhotoId(), Constants.USER_PHOTO_SMALL);
				Bitmap bitmap;

				if (imagePath.exists())
				{
					bitmap = BitmapCommands.Path.build(imagePath.getAbsolutePath(), Constants.USER_PHOTO_SMALL, Constants.USER_PHOTO_SMALL);

				} else
				{
					bitmap = client.loadFile(deed.getUserPhotoId(), Constants.USER_PHOTO_SMALL);
					FileManager.saveImage(imagePath, bitmap);
				}

				Drawable drawable = new BitmapDrawable(ac.getResources(), bitmap);
				DeedItemOverlay overlay = new DeedItemOverlay(drawable, ac, new OnTapEvent() {

					@Override
					public void OnTap()
					{
						DeedData deedData = deed;
						DataHolder.INSTANCE.setDeedData(deedData);
						startDeedDetailActivity();
					}
				});

				int lat = (int) (deed.getLatitude() * 1E6);
				int lon = (int) (deed.getLongtitude() * 1E6);

				if(lat == 0 || lon == 0)
				{
					return null;
				}

				GeoPoint point = new GeoPoint(lat, lon);
				LayoutInflater inflater = (LayoutInflater) ac.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				FrameLayout markerLayout = (FrameLayout) inflater.inflate(R.layout.marker, null);

				ImageView imgPhoto = (ImageView) markerLayout.findViewById(R.id.imgPhoto);
				imgPhoto.setImageDrawable(drawable);

				CustomItem overlayitem = new CustomItem(point, deed.getTitle(), deed.getDescription(), markerLayout);
				overlay.addOverlay(overlayitem);

				return overlay;
			}

			@Override
			protected void onPostExecute(DeedItemOverlay result)
			{
				if(result != null)
				{
					addDeedToMap(result);
				}
			};

		}.execute();
	}

	private void startDeedDetailActivity()
	{
		Intent intent = new Intent(ac, DeedDetailActivity.class);
		ac.startActivity(intent);
	}

}
