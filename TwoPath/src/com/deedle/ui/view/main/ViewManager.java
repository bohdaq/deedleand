package com.deedle.ui.view.main;

import java.util.ArrayList;
import java.util.List;

import com.deedle.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

public class ViewManager
{
	private final List<BaseView> viewList = new ArrayList<BaseView>();
	private final LinearLayout viewHolder;

	public ViewManager(Activity ac)
	{
		LayoutInflater inflater = ac.getLayoutInflater();
		viewHolder = (LinearLayout) inflater.inflate(R.layout.main_content, null);
	}

	public void add(BaseView view)
	{
		viewList.add(view);
		viewHolder.addView(view.getView());
		view.getView().setVisibility(View.GONE);
	}

	public View getView()
	{
		return viewHolder;
	}

	public BaseView getView(ViewId viewId)
	{
		BaseView result = null;

		for (BaseView view : viewList)
		{
			if (viewId == view.getId())
			{
				result = view;
			}
		}
		return result;
	}

	public boolean isActive(ViewId viewId)
	{
		boolean result = false;

		for (BaseView view : viewList)
		{
			if (viewId == view.getId() && view.getView().getVisibility() == View.VISIBLE)
			{
				result = true;
			}
		}

		return result;
	}

	public void activate(ViewId viewId)
	{
		for (BaseView view : viewList)
		{
			if (viewId == view.getId())
			{
				view.getView().setVisibility(View.VISIBLE);
				view.onRefresh();
			} else
			{
				view.getView().setVisibility(View.GONE);
			}
		}
	}
}
