package com.deedle.ui.view.main;

import com.deedle.ui.view.HeaderView;

import android.view.View;

public abstract class BaseView
{

	private HeaderView headerView;

	public abstract ViewId getId();

	public abstract View getView();

	public void onRefresh()
	{

	}

	public HeaderView getHeaderView()
	{
		return headerView;
	}

	public void setHeaderView(HeaderView headerView)
	{
		this.headerView = headerView;
	}
}
