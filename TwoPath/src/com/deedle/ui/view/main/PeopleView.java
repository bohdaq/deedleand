package com.deedle.ui.view.main;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import com.dd.utils.log.L;
import com.deedle.R;
import com.deedle.adapter.FollowingUserAdapter;
import com.deedle.api.v2.user.UserClient;
import com.deedle.api.v2.user.UserData;
import com.deedle.data.DataHolder;

public class PeopleView extends BaseView
{
	private final UserData userData = DataHolder.INSTANCE.getUserData();

	private final Activity ac;
	private View view;

	public PeopleView(Activity ac)
	{
		this.ac = ac;

		startUserLoading();
	}

	@Override
	public View getView()
	{
		LayoutInflater inflater = ac.getLayoutInflater();

		if (view == null)
		{
			view = inflater.inflate(R.layout.people, null);
		}

		return view;
	}

	@Override
	public ViewId getId()
	{
		return ViewId.PEOPLE;
	}

	@Override
	public void onRefresh()
	{
		startUserLoading();
	}

	private void startUserLoading()
	{
		new AsyncTask<Void, Void, List<UserData>>() {

			@Override
			protected List<UserData> doInBackground(Void... params)
			{
				UserClient client = new UserClient();
				List<UserData> userList = new ArrayList<UserData>();
				List<String> followingEmailList = userData.getFollowingEmailList();

				for (String email : followingEmailList)
				{
					if(email.trim().length() > 0)
					{
						UserData data = client.info(email);
						userList.add(data);
					}
				}

				return userList;
			}

			@Override
			protected void onPostExecute(List<UserData> userList)
			{
				initView(userList);
			};

		}.execute();
	}

	private void initView(List<UserData> userList)
	{
		ListView listView = (ListView) getView().findViewById(R.id.listView);
		FollowingUserAdapter adapter = (FollowingUserAdapter) listView.getAdapter();

		if(adapter == null)
		{
			adapter = new FollowingUserAdapter(ac, userList, userData);
			listView.setAdapter(adapter);
		}else
		{
			adapter.getUserList().clear();
			adapter.getUserList().addAll(userList);
			adapter.notifyDataSetChanged();
		}

	}

}
