package com.deedle.ui.view;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.deedle.R;

public class MenuView implements RadioGroup.OnCheckedChangeListener
{
	public interface MenuItemClickListener
	{
		public void onUserDetailSelected();

		public void onDeedFeedSelected();

		public void onPeopleSelected();

		public void onGeneralSelected();

		public void onFollowingSelected();

		public void onAllSelected();

		public void onGoodSelected();

		public void onBadSelected();

		public void onGridSelected();

		public void onListSelected();

		public void onMapSelected();

	}

	private final View viewHolder;
	private final MenuItemClickListener listener;
	private boolean isClosed;

	public MenuView(Activity ac, MenuItemClickListener listener)
	{
		this.listener = listener;
		LayoutInflater inflater = ac.getLayoutInflater();
		viewHolder = inflater.inflate(R.layout.main_menu, null);

		initListeners();
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId)
	{
		if (isClosed())
		{
			return;
		}

		if (checkedId == R.id.rbPeople)
		{
			listener.onPeopleSelected();
		} else if (checkedId == R.id.rbUserDetail)
		{
			listener.onUserDetailSelected();
		} else if (checkedId == R.id.rbDeedFeed)
		{
			listener.onDeedFeedSelected();
		}  else if (checkedId == R.id.rbGeneral)
		{
			listener.onGeneralSelected();
		} else if (checkedId == R.id.rbFollowing)
		{
			listener.onFollowingSelected();
		} else if (checkedId == R.id.rbGood)
		{
			listener.onGoodSelected();
		} else if (checkedId == R.id.rbBad)
		{
			listener.onBadSelected();
		} else if (checkedId == R.id.rbAll)
		{
			listener.onAllSelected();
		}else if (checkedId == R.id.rbGrid)
		{
			listener.onGridSelected();
		}else if (checkedId == R.id.rbList)
		{
			listener.onListSelected();
		}else if (checkedId == R.id.rbMap)
		{
			listener.onMapSelected();
		}

	}

	private void initListeners()
	{
		RadioGroup rgPages = (RadioGroup) getView().findViewById(R.id.rgPages);
		rgPages.setOnCheckedChangeListener(this);

		RadioGroup rgFilter = (RadioGroup) getView().findViewById(R.id.rgFilter);
		rgFilter.setOnCheckedChangeListener(this);

		RadioGroup rgDeeds = (RadioGroup) getView().findViewById(R.id.rgDeeds);
		rgDeeds.setOnCheckedChangeListener(this);

		RadioGroup rgView = (RadioGroup) getView().findViewById(R.id.rgView);
		rgView.setOnCheckedChangeListener(this);
	}

	public void initUserInfo(String name, Bitmap bitmap)
	{
		RadioButton rbUserDetail = (RadioButton) getView().findViewById(R.id.rbUserDetail);
		rbUserDetail.setText(name);
	}

	public View getView()
	{
		return viewHolder;
	}

	public boolean isClosed()
	{
		return isClosed;
	}

	public void setClosed(boolean isClosed)
	{
		this.isClosed = isClosed;
	}
}
