package com.deedle.ui.view;

import java.io.File;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.deedle.R;
import com.deedle.api.v2.deed.DeedData;
import com.deedle.api.v2.user.UserClient;
import com.deedle.api.v2.user.UserData;
import com.deedle.data.Constants;
import com.deedle.loader.ImageLoader;
import com.deedle.utils.DateHelper;
import com.deedle.utils.FileManager;

public class DeedDetailView implements OnCheckedChangeListener
{
	private final View view;
	private final Context context;
	private final DeedData deedData;
	private final UserData userData;

	public DeedDetailView(Context context, DeedData deedData, UserData userData)
	{
		this.context = context;
		this.deedData = deedData;
		this.userData = userData;

		LayoutInflater inflater = LayoutInflater.from(context);
		view = inflater.inflate(R.layout.deed_detail_view, null);

		initView(context);
		loadUserInfo();

	}

	public View getView()
	{
		return view;
	}

	@Override
	public void onCheckedChanged(CompoundButton view, boolean isChecked)
	{
		if (isChecked)
		{
			followTask();
		} else
		{
			unfollowTask();
		}
	}

	protected void loadUserPhoto(UserData userData)
	{
		ImageView imageView = (ImageView) getView().findViewById(R.id.imgPhoto);
		FileManager fileManager = new FileManager(context);
		File imagePath = fileManager.getDeedPhoto(userData.getPhotoId(), Constants.USER_PHOTO_SMALL);

		ImageLoader loader = new ImageLoader();
		loader.load(imageView, imagePath, userData.getPhotoId(), Constants.USER_PHOTO_SMALL);

	}

	private void followTask()
	{
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params)
			{
				UserClient client = new UserClient();
				client.follow(userData.getEmail(), deedData.getEmail());
				userData.getFollowingEmailList().add(deedData.getEmail());

				return null;
			}

		}.execute();
	}

	private void unfollowTask()
	{
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params)
			{
				UserClient client = new UserClient();
				client.unfollow(userData.getEmail(), deedData.getEmail());
				userData.getFollowingEmailList().remove(deedData.getEmail());

				return null;
			}

		}.execute();
	}

	private void loadUserInfo()
	{
		new AsyncTask<Void, Void, UserData>() {

			@Override
			protected UserData doInBackground(Void... params)
			{
				UserClient client = new UserClient();
				UserData userData = client.info(deedData.getEmail());

				return userData;
			}

			@Override
			protected void onPostExecute(UserData userData)
			{
				updateUserInfo(userData);
				loadUserPhoto(userData);
			};
		}.execute();

	}

	private void updateUserInfo(UserData userData)
	{
		TextView txtName = (TextView) getView().findViewById(R.id.txtName);
		txtName.setText(userData.getName());

		TextView txtLocation = (TextView) getView().findViewById(R.id.txtLocation);
		txtLocation.setText(userData.getCity());
	}

	private void initView(Context context)
	{
		CheckBox cbFollow = (CheckBox) getView().findViewById(R.id.cbFollow);

		if(userData.getFollowingEmailList().contains(deedData.getEmail()))
		{
			cbFollow.setChecked(true);
		}

		if(userData.getEmail().equalsIgnoreCase(deedData.getEmail()))
		{
			cbFollow.setVisibility(View.GONE);
		}

		cbFollow.setOnCheckedChangeListener(this);

		TextView txtTitle = (TextView) getView().findViewById(R.id.txtTitle);
		txtTitle.setText(deedData.getTitle());

		TextView txtDescription = (TextView) getView().findViewById(R.id.txtDescription);
		txtDescription.setText(deedData.getDescription());

		TextView txtLocationTime = (TextView) getView().findViewById(R.id.txtLocationTime);

		if (deedData.getCity() == null)
		{
			txtLocationTime.setText(DateHelper.getDateFrom(deedData.getDate()));
		} else
		{
			txtLocationTime.setText(deedData.getCity() + ", " + DateHelper.getDateFrom(deedData.getDate()));
		}

	}
}
