package com.deedle.ui.view;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.deedle.R;

public class HeaderView
{
	private final View viewHolder;
	private Animation refreshAnimation;

	public HeaderView(Activity ac)
	{
		LayoutInflater inflater = ac.getLayoutInflater();
		viewHolder = inflater.inflate(R.layout.header, null);

		refreshAnimation = AnimationUtils.loadAnimation(getView().getContext(), R.anim.refresh);
		refreshAnimation.setRepeatCount(Animation.INFINITE);
	}

	public HeaderView(View view)
	{
		viewHolder = view;
	}

	public View getView()
	{
		return viewHolder;
	}

	public void clean()
	{
		ImageView imgOk = (ImageView) getView().findViewById(R.id.imgOk);
		ImageView imgRefresh = (ImageView) getView().findViewById(R.id.imgRefresh);
		ImageView imgAddNewDeed = (ImageView) getView().findViewById(R.id.imgAddNewDeed);
		ImageView imgLogo = (ImageView) getView().findViewById(R.id.imgLogo);
		TextView txtTitle = (TextView) getView().findViewById(R.id.txtTitle);

		imgOk.setVisibility(View.GONE);
		imgRefresh.setVisibility(View.GONE);
		imgAddNewDeed.setVisibility(View.GONE);
		imgLogo.setVisibility(View.GONE);
		txtTitle.setVisibility(View.GONE);
	}

	public void createTitle(String text)
	{
		TextView txtTitle = (TextView) getView().findViewById(R.id.txtTitle);
		txtTitle.setVisibility(View.VISIBLE);
		txtTitle.setText(text);
	}

	public void createRefreshButton(OnClickListener listener)
	{
		ImageView imgRefresh = (ImageView) getView().findViewById(R.id.imgRefresh);
		imgRefresh.setVisibility(View.VISIBLE);
		imgRefresh.setOnClickListener(listener);
	}

	public void startRefreshAnimation()
	{
		ImageView imgRefresh = (ImageView) getView().findViewById(R.id.imgRefresh);

		if (imgRefresh.getVisibility() == View.VISIBLE)
		{
			imgRefresh.startAnimation(refreshAnimation);
		}

	}

	public void stopRefreshAnimation()
	{
		ImageView imgRefresh = (ImageView) getView().findViewById(R.id.imgRefresh);
		imgRefresh.clearAnimation();
		refreshAnimation.cancel();
	}

	public void createAddNewDeedButton(OnClickListener listener)
	{
		ImageView imgAddNewDeed = (ImageView) getView().findViewById(R.id.imgAddNewDeed);
		imgAddNewDeed.setVisibility(View.VISIBLE);
		imgAddNewDeed.setOnClickListener(listener);
	}

	public void createOkButton(OnClickListener listener)
	{
		ImageView imgOk = (ImageView) getView().findViewById(R.id.imgOk);
		imgOk.setVisibility(View.VISIBLE);
		imgOk.setOnClickListener(listener);
	}

	public void removeOkButton()
	{
		ImageView imgOk = (ImageView) getView().findViewById(R.id.imgOk);
		imgOk.setVisibility(View.GONE);
	}

	public void createLogo()
	{
		ImageView imgLogo = (ImageView) getView().findViewById(R.id.imgLogo);
		imgLogo.setBackgroundResource(R.drawable.logo);
		imgLogo.setVisibility(View.VISIBLE);
	}

	public void createLogo(OnClickListener listener)
	{
		ImageView imgLogo = (ImageView) getView().findViewById(R.id.imgLogo);
		imgLogo.setBackgroundResource(R.drawable.logo_back);
		imgLogo.setVisibility(View.VISIBLE);
		imgLogo.setOnClickListener(listener);
	}
}
