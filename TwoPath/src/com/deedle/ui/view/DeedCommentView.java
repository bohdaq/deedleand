package com.deedle.ui.view;

import java.io.File;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.deedle.R;
import com.deedle.adapter.DeedCommentAdapter;
import com.deedle.api.v2.Result;
import com.deedle.api.v2.comment.CommentClient;
import com.deedle.api.v2.comment.CommentData;
import com.deedle.api.v2.deed.DeedData;
import com.deedle.api.v2.user.UserData;
import com.deedle.data.Constants;
import com.deedle.loader.ImageLoader;
import com.deedle.utils.FileManager;
import com.deedle.utils.SystemUtils;

public class DeedCommentView implements TextWatcher, OnClickListener
{
	private final View view;
	private final Activity ac;
	private final UserData userData;
	private final DeedData deedData;

	public DeedCommentView(Activity ac, DeedData deedData, UserData userData)
	{
		this.ac = ac;
		this.deedData = deedData;
		this.userData = userData;

		LayoutInflater inflater = LayoutInflater.from(ac);
		view = inflater.inflate(R.layout.deed_comment, null);

		initView(ac);
		loadCommentList();
		loadUserPhoto();
		disableSendButton();
	}

	public View getView()
	{
		return view;
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
	{
		String text = s.toString().trim();

		if (text.length() > 2)
		{
			enableSendButton();
		} else
		{
			disableSendButton();
		}
	}

	@Override
	public void onClick(View view)
	{
		int id = view.getId();

		if (id == R.id.btnSend)
		{
			sendCommentTask();
		}
	}

	@Override
	public void afterTextChanged(Editable text)
	{

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after)
	{

	}

	private void initView(Context context)
	{
		EditText inputComment = (EditText) getView().findViewById(R.id.inputComment);
		inputComment.addTextChangedListener(this);

		Button btnSend = (Button) getView().findViewById(R.id.btnSend);
		btnSend.setOnClickListener(this);
	}

	private String getCommentText()
	{
		EditText inputComment = (EditText) getView().findViewById(R.id.inputComment);
		String commentText = inputComment.getText().toString().trim();

		return commentText;
	}

	private void clearCommentText()
	{
		EditText inputComment = (EditText) getView().findViewById(R.id.inputComment);
		inputComment.setText(null);
	}

	private void disableSendButton()
	{
		Button btnSend = (Button) getView().findViewById(R.id.btnSend);
		btnSend.setEnabled(false);
	}

	private void enableSendButton()
	{
		Button btnSend = (Button) getView().findViewById(R.id.btnSend);
		btnSend.setEnabled(true);
	}

	private void loadUserPhoto()
	{
		ImageView imageView = (ImageView) getView().findViewById(R.id.imgPhoto);
		FileManager fileManager = new FileManager(ac);
		File imagePath = fileManager.getDeedPhoto(userData.getPhotoId(), Constants.USER_PHOTO_SMALL);

		ImageLoader loader = new ImageLoader();
		loader.load(imageView, imagePath, userData.getPhotoId(), Constants.USER_PHOTO_SMALL);

	}

	private void updateCommentView(List<CommentData> commentList)
	{
		ListView commentListView = (ListView) getView().findViewById(R.id.commentListView);
		DeedCommentAdapter adapter = new DeedCommentAdapter(ac, commentList);
		commentListView.setAdapter(adapter);

	}

	private void updateCommentView(CommentData comment)
	{
		ListView commentListView = (ListView) getView().findViewById(R.id.commentListView);
		DeedCommentAdapter adapter = (DeedCommentAdapter) commentListView.getAdapter();
		List<CommentData> commentList = adapter.getCommentList();
		commentList.add(comment);
		adapter.notifyDataSetChanged();

	}

	private void loadCommentList()
	{
		new AsyncTask<Void, Void, List<CommentData>>() {

			@Override
			protected List<CommentData> doInBackground(Void... params)
			{
				CommentClient client = new CommentClient();
				List<CommentData> commentList = client.getComments(deedData.getPhotoId());

				return commentList;
			}

			@Override
			protected void onPostExecute(List<CommentData> commentList)
			{
				updateCommentView(commentList);
			};

		}.execute();

	}

	private void sendCommentTask()
	{

		final CommentData comment = new CommentData();
		comment.setComment(getCommentText());
		comment.setDeedId(deedData.getPhotoId());
		comment.setEmail(userData.getEmail());
		comment.setUserPhotoId(userData.getPhotoId());
		comment.setUserName(userData.getName());
		comment.setDate(Calendar.getInstance().getTimeInMillis());

		new AsyncTask<Void, Void, Result>() {

			@Override
			protected void onPreExecute()
			{
				clearCommentText();
				disableSendButton();
				updateCommentView(comment);
				SystemUtils.hideKeyboard(ac);
			};

			@Override
			protected Result doInBackground(Void... params)
			{
				CommentClient client = new CommentClient();
				Result result = client.comment(comment);

				return result;
			}

		}.execute();
	}
}
