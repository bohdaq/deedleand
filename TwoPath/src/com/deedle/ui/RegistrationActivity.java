package com.deedle.ui;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore.MediaColumns;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.dd.commands.BitmapCommands;
import com.dd.ui.DialogView;
import com.dd.utils.log.L;
import com.deedle.R;
import com.deedle.api.drive.DriveClient;
import com.deedle.api.v2.Result;
import com.deedle.api.v2.user.NewUserData;
import com.deedle.api.v2.user.UserClient;
import com.deedle.api.v2.user.UserData;
import com.deedle.data.Constants;
import com.deedle.data.DataHolder;
import com.deedle.data.Pref;
import com.deedle.ui.view.HeaderView;
import com.deedle.utils.FileManager;
import com.deedle.utils.LocationHelper;
import com.deedle.utils.LocationUtils;
import com.google.api.services.drive.model.File;

public class RegistrationActivity extends BaseActivity implements OnClickListener, TextWatcher, LocationListener, OnCancelListener
{
	private static final int MINIMUM_LETTERS = 2;
	private static final int REQ_CODE_PICK_IMAGE = 1000;
	private final DialogView dialog = new DialogView();

	private final NewUserData newUser = new NewUserData();
	private final UserData userData = DataHolder.INSTANCE.getUserData();
	private LocationHelper locationHelper;
	private  java.io.File userPhoto;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registration);

		if (userData == null)
		{
			ActivityManager.startLoginActivity(this);
			finish();
			return;
		}

		locationHelper = new LocationHelper(this, this, this);

		FileManager fileManager = new FileManager(this);
		userPhoto = fileManager.getUserPhoto();
		userPhoto.delete();

		// TODO add loading user info from network
		// int currentAPIVersion = android.os.Build.VERSION.SDK_INT;

		// if (currentAPIVersion >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
		// {
		// loadUserDataFromContact();
		// }

		initHeader();
		initView();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent)
	{
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

		if (requestCode == REQ_CODE_PICK_IMAGE && resultCode == RESULT_OK)
		{
			Uri selectedImage = imageReturnedIntent.getData();
			String[] filePathColumn = { MediaColumns.DATA };

			Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String filePath = cursor.getString(columnIndex);
			cursor.close();

			saveAvatarToCacheTask(filePath);

		}
	}

	@Override
	public void onClick(View v)
	{
		int id = v.getId();
		if (id == R.id.imgOk)
		{
			saveDataTask();
		} else if (id == R.id.btnDetermLocation)
		{
			determineLocation();
		} else if (id == R.id.imgPhoto || id == R.id.btnAddYourPhoto)
		{
			startGalleryIntent();
		}

	}

	@Override
	public void afterTextChanged(Editable text)
	{
		validate();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
	{
		// unused
	}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
	{
		// unused
	}

	@Override
	public void onLocationChanged(Location location)
	{
		if(location == null)
		{
			location = new Location(LocationManager.NETWORK_PROVIDER);
		}

		L.i("Location: " + location.getLatitude() + " : " + location.getLongitude());

		locationHelper.stopCheckingLocation();

		Address address = LocationUtils.getAddress(this, location);

		String country = null;
		String city = null;
		String adminArea = null;

		if (address != null)
		{
			country = address.getCountryName();
			city = address.getLocality();
			adminArea = address.getAdminArea();
		}

		if (country != null)
		{
			newUser.setCountry(country);
		}

		if (city != null)
		{
			newUser.setCity(city);
		} else if (adminArea != null)
		{
			newUser.setCity(adminArea);
		}

		updateCityView(newUser.getCity());
		updateCountryView(newUser.getCountry());
	}

	@Override
	public void onProviderDisabled(String arg0)
	{
		// unused

	}

	@Override
	public void onProviderEnabled(String arg0)
	{
		// unused

	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2)
	{
		// unused

	}

	@Override
	public void onCancel(DialogInterface dialog)
	{
		locationHelper.stopCheckingLocation();
	}

	@Override
	public void onBackPressed()
	{
		exit();
		super.onBackPressed();
	}


	// @TargetApi(14)
	// private void loadUserDataFromContact()
	// {
	// Cursor cursor = null;
	//
	// try
	// {
	//
	// cursor = getContentResolver().query(ContactsContract.Profile.CONTENT_URI, null, null, null, null);
	// cursor.moveToFirst();
	// int count = cursor.getCount();
	// int position = cursor.getPosition();
	// if (count == 1 && position == 0)
	// {
	// String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Profile.DISPLAY_NAME));
	// String photoUri = cursor.getString(cursor.getColumnIndex(ContactsContract.Profile.PHOTO_URI));
	//
	// if (photoUri != null)
	// {
	// Bitmap bitmap = null;
	//
	// try
	// {
	// bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(photoUri));
	// } catch (FileNotFoundException e)
	// {
	// L.e(e.toString());
	// } catch (IOException e)
	// {
	// L.e(e.toString());
	// }
	//
	// FileUtils.saveImage(FileUtils.USER_PHOTO, bitmap);
	// }
	//
	// }
	//
	// } finally
	// {
	// if (cursor != null)
	// {
	// cursor.close();
	// }
	//
	// }
	//
	// }

	private void validate()
	{
		EditText inputName = (EditText) findViewById(R.id.inputName);
		EditText inputCountry = (EditText) findViewById(R.id.inputCountry);
		EditText inputCity = (EditText) findViewById(R.id.inputCity);

		String name = inputName.getText().toString().trim();
		String country = inputCountry.getText().toString().trim();
		String city = inputCity.getText().toString().trim();

		boolean titleValid = name.length() >= MINIMUM_LETTERS;
		boolean countryValid = country.length() >= MINIMUM_LETTERS;
		boolean cityValid = city.length() >= MINIMUM_LETTERS;
		boolean imageValid = userPhoto.exists();

		if (titleValid && countryValid && cityValid && imageValid)
		{
			newUser.setName(name);
			newUser.setCity(city);
			newUser.setCountry(country);

			showOkButton();
		} else
		{
			hideOkButton();
		}

	}

	private void determineLocation()
	{
		locationHelper.checkNetworkLocation();
	}

	private void saveDataTask()
	{
		new AsyncTask<Void, File, Result>() {

			@Override
			protected void onPreExecute()
			{
				dialog.showProgressDialog(RegistrationActivity.this, getString(R.string.saving_data));
			};

			@Override
			protected Result doInBackground(Void... params)
			{

				// saveDefaultAvatarToCache();

				DriveClient driveClient = new DriveClient();

				String email = Pref.getGoogleAccount(RegistrationActivity.this).getEmail();
				String authtoken = Pref.getGoogleAccount(RegistrationActivity.this).getAuthtoken();
				String folderId = driveClient.createFolder(authtoken, Constants.Drive.API_KEY);

				// upload user photo to drive root folder
				File avatarFile = driveClient.uploadFile(authtoken, Constants.Drive.API_KEY, userPhoto.getAbsolutePath(),
						Constants.Drive.AVATAR, Constants.Drive.AVATAR, folderId);

				newUser.setEmail(email);
				newUser.setPhotoId(avatarFile.getId());
				newUser.setFolderId(folderId);

				userData.copy(newUser);

				// save data to server side
				UserClient client = new UserClient();
				Result result = client.update(newUser);

				return result;
			}

			@Override
			protected void onPostExecute(Result result)
			{
				dialog.closeProgress();

				if (Result.SUCCESS == result)
				{
					startMainActivity();
					finish();
				} else
				{
					Toast.makeText(RegistrationActivity.this, getString(R.string.oops), Toast.LENGTH_LONG).show();
				}
			}
		}.execute();

	}

	private void saveAvatarToCacheTask(final String filePath)
	{
		new AsyncTask<Void, Bitmap, Bitmap>() {
			@Override
			protected void onPreExecute()
			{
				dialog.showProgressDialog(RegistrationActivity.this, getString(R.string.loading_image));
			};

			@Override
			protected Bitmap doInBackground(Void... params)
			{
				Bitmap bitmap = BitmapCommands.Path.build(filePath, Constants.USER_PHOTO, Constants.USER_PHOTO);
				FileManager.saveImage(userPhoto, bitmap);

				return bitmap;
			}

			@Override
			protected void onPostExecute(Bitmap bitmap)
			{
				updateUserPhotoView(bitmap);
				validate();

				dialog.closeProgress();
			};

		}.execute();

	}

	private void startGalleryIntent()
	{
		Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(intent, REQ_CODE_PICK_IMAGE);
	}

	private void updateUserPhotoView(Bitmap bitmap)
	{
		ImageView imgPhoto = (ImageView) findViewById(R.id.imgPhoto);
		imgPhoto.setImageBitmap(bitmap);
	}

	private void updateCityView(String text)
	{
		EditText inputCity = (EditText) findViewById(R.id.inputCity);
		inputCity.setText(text);
	}

	private void updateCountryView(String text)
	{
		EditText inputCountry = (EditText) findViewById(R.id.inputCountry);
		inputCountry.setText(text);
	}

	private void initView()
	{
		Button btnAddYourPhoto = (Button) findViewById(R.id.btnAddYourPhoto);
		btnAddYourPhoto.setOnClickListener(this);

		Button btnDetermLocation = (Button) findViewById(R.id.btnDetermLocation);
		btnDetermLocation.setOnClickListener(this);

		ImageView imgPhoto = (ImageView) findViewById(R.id.imgPhoto);
		imgPhoto.setOnClickListener(this);

		if (userPhoto.exists())
		{
			Bitmap bitmap = BitmapCommands.Path.build(userPhoto.getAbsolutePath());
			updateUserPhotoView(bitmap);
		}

		EditText inputName = (EditText) findViewById(R.id.inputName);
		inputName.setText(userData.getName());
		inputName.addTextChangedListener(this);

		EditText inputCountry = (EditText) findViewById(R.id.inputCountry);
		inputCountry.setText(userData.getCountry());
		inputCountry.addTextChangedListener(this);

		EditText inputCity = (EditText) findViewById(R.id.inputCity);
		inputCity.setText(userData.getCity());
		inputCity.addTextChangedListener(this);

	}

	private void startMainActivity()
	{
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
	}

	private void exit()
	{
		Pref.clearGoogleAccount(RegistrationActivity.this);
		finish();
	}

	private void initHeader()
	{
		HeaderView header = new HeaderView(findViewById(R.id.header));
		header.clean();
		header.createTitle(getString(R.string.configure_your_profile));
	}

	private void showOkButton()
	{
		HeaderView header = new HeaderView(findViewById(R.id.header));
		header.createOkButton(this);
	}

	private void hideOkButton()
	{
		HeaderView header = new HeaderView(findViewById(R.id.header));
		header.removeOkButton();
	}
}
